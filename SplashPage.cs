﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace GlobalNet
{
	[Activity(MainLauncher = true, NoHistory = true, Theme = "@style/Theme.Splash", ScreenOrientation = ScreenOrientation.Portrait)]
	public class SplashPage : Activity
	{
		bool notification;
		string NotificationString;

		ImageView img;

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			SetContentView(Resource.Layout.SplashPageLayout);

			notification = Intent.GetBooleanExtra("notification", false);


			if (notification)
			{

				NotificationString = Intent.GetStringExtra("NotificationString");

			}

			ThreadPool.QueueUserWorkItem(o => LoadActivity());

		}
		public void LoadActivity()
		{
			Thread.Sleep(1000);
			RunOnUiThread(() =>
			{

				var intent = new Intent(this, typeof(MainActivity));

				if (notification)
				{
					intent.PutExtra("notification", true);
					intent.PutExtra("NotificationString", NotificationString);
				}

				StartActivity(intent);

				Finish();
			});
		}
	}
}
