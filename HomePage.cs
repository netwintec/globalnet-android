﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Animation;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace GlobalNet
{
	[Activity(Label = "HomePage", ScreenOrientation = ScreenOrientation.Portrait)]
	public class HomePage : Activity
	{
		Typeface tfCL, tfCB;

		RelativeLayout MenuClick;
		ImageView MenuImg;

		TextView SocietyTitle, SocietyCode;

        LinearLayout FatturaBtn, ContrattiBtn, DatiBtn, NotificheBtn;
		TextView FatturaTxt, ContrattiTxt, DatiTxt, NotificheTxt;

		// MENU VARIABLE 

		RelativeLayout MenuView;
		RelativeLayout Menu; // 200 x 335

        LinearLayout FatturaMenu, ContrattiMenu, DatiMenu, NotificheMenu;
        TextView FatturaTxtMenu, ContrattiTxtMenu, DatiTxtMenu, NotificheTxtMenu;

		ValueAnimator MenuValueAnimator;

		bool MenuOpen = false;

		// NOtification VARIABLE 

		RelativeLayout NotView;
		RelativeLayout ChiudiL, VediL;

		TextView ChiudiTxt, VediTxt, NotTitTxt, NotTxt;
		ImageView NotImg;

		ValueAnimator NotValueAnimator;


		bool notification;
		string NotificationString;
	
		//bool MenuOpen = false;
		public static HomePage Instance { private set; get;}

		public bool isVisible = true;

		protected override void OnStop()
		{
			base.OnStop();
			isVisible = false;
		}

		protected override void OnResume()
		{
			base.OnResume();

			isVisible = true;

			GlobalStructureInstance.Instance.SetPage(PageName.Home);

			RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)NotView.LayoutParameters;
			if (layoutParams.BottomMargin < 0 && GlobalStructureInstance.Instance.NotificaObj != null)
				DisplayNot(GlobalStructureInstance.Instance.NotificaObj);


			if (layoutParams.BottomMargin == 0 && GlobalStructureInstance.Instance.NotificaObj == null)
			{
				RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams)NotView.LayoutParameters;
				layoutParams2.BottomMargin = -PixelsToDp(60);
				NotView.LayoutParameters = layoutParams2;
			}
		}

		protected override void OnCreate(Bundle savedInstanceState)
		{
			
			base.OnCreate(savedInstanceState);

			//MainActivity.Instance.LoadView.Visibility = ViewStates.Gone;

			Window.RequestFeature(WindowFeatures.NoTitle);

			SetContentView(Resource.Layout.HomePage);

			HomePage.Instance = this;
			GlobalStructureInstance.Instance.SetPage(PageName.Home);

			notification = Intent.GetBooleanExtra("notification", false);

			if (notification)
			{
				NotificationString = Intent.GetStringExtra("NotificationString");
			}

			tfCL = Typeface.CreateFromAsset(Assets, "fonts/OpenSans_CondLight.ttf");
			tfCB = Typeface.CreateFromAsset(Assets, "fonts/OpenSans_CondBold.ttf");

			//NAVIGATION BAR MENU
			MenuClick = FindViewById<RelativeLayout>(Resource.Id.MenuClick);
			MenuImg = FindViewById<ImageView>(Resource.Id.MenuIcon);

			MenuClick.Click += delegate
			{

				OpenMenu();

			};

			SocietyTitle = FindViewById<TextView>(Resource.Id.SocietyName);
			SocietyCode = FindViewById<TextView>(Resource.Id.SocietyCode);

			SocietyTitle.Typeface = tfCL;
			SocietyCode.Typeface = tfCL;

			string NomeCliente = "Ragione sociale";
			string CodCliente = "Codice Cliente";
            if (GlobalStructureInstance.Instance.listAnagrafica.Count != 0)
            {

                var ao = GlobalStructureInstance.Instance.listAnagrafica[0];

				NomeCliente = ao.RagioneSociale;
                CodCliente = "Codice Cliente: " + ao.CodiceCliente;

            }
			SocietyTitle.Text = NomeCliente;
			SocietyCode.Text = CodCliente;

			FatturaBtn = FindViewById<LinearLayout>(Resource.Id.FatturaButton);
			ContrattiBtn = FindViewById<LinearLayout>(Resource.Id.ContrattiButton);
			DatiBtn = FindViewById<LinearLayout>(Resource.Id.DatiButton);
            NotificheBtn = FindViewById<LinearLayout>(Resource.Id.NotificheButton);

			FatturaTxt = FindViewById<TextView>(Resource.Id.FatturaTxt);
			ContrattiTxt = FindViewById<TextView>(Resource.Id.ContrattiTxt);
			DatiTxt = FindViewById<TextView>(Resource.Id.DatiTxt);
            NotificheTxt = FindViewById<TextView>(Resource.Id.NotificheTxt);

			FatturaTxt.Typeface = tfCB;
			ContrattiTxt.Typeface = tfCB;
			DatiTxt.Typeface = tfCB;
            NotificheTxt.Typeface = tfCB;

			FatturaBtn.Click += delegate {

				var intent = new Intent(this, typeof(FatturePage));
				this.StartActivity(intent);

			};

			ContrattiBtn.Click += delegate {

				var intent = new Intent(this, typeof(ContrattiPage));
				this.StartActivity(intent);

			};

			DatiBtn.Click += delegate {

				var intent = new Intent(this, typeof(InfoDataPage));
				this.StartActivity(intent);

			};

			NotificheBtn.Click += delegate
			{

				var intent = new Intent(this, typeof(NotifichePage));
				this.StartActivity(intent);			

			};


			InitMenu();

			InitNot();


			if (notification) {

				NotificheObject Not;

				try
				{
					Not = JsonConvert.DeserializeObject<NotificheObject>(NotificationString);

					if (Not.Type == NotificheType.Fattura)
					{
						var intent = new Intent(this, typeof(FatturePage));
						intent.PutExtra("saledocId", Not.Id_Object);
						this.StartActivity(intent);
					}

					if (Not.Type == NotificheType.Contratto)
					{
						var intent = new Intent(this, typeof(ContrattiPage));
						intent.PutExtra("contrattiNumber", Not.Id_Object);
						this.StartActivity(intent);
					}

					if (Not.Type == NotificheType.ScadenzaFattura)
					{
						var intent = new Intent(this, typeof(FatturePage));
						intent.PutExtra("saledocId", Not.Id_Object);
						this.StartActivity(intent);
					}
				
					notification = false;
					NotificationString = "";

				}
				catch (Exception e)
				{
					notification = false;
					NotificationString = "";
				}
					
			}

            if (!GlobalStructureInstance.Instance.SendToken && !string.IsNullOrEmpty(GlobalStructureInstance.Instance.APNToken))
            {
                SendToken(GlobalStructureInstance.Instance.APNToken, 0);
            }

		}

		public override void OnBackPressed()
		{
			//base.OnBackPressed();

			if (MenuOpen)
			{

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

			}
			else { 
			
				Intent startMain = new Intent(Intent.ActionMain);
				startMain.AddCategory(Intent.CategoryHome);
				startMain.SetFlags(ActivityFlags.NewTask);
				StartActivity(startMain);


			}

		}


		public void OpenMenu() { 
		
			if (MenuOpen)
			{
				
				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

			}
			else {

				MenuView.Visibility = ViewStates.Visible;
				MenuImg.SetImageResource(Resource.Drawable.MenuSpentoIcon);

				MenuValueAnimator.SetIntValues(0, 100);
				MenuValueAnimator.Start();
			
			}

		}

		public void InitMenu() { 
		
			// GESTIONE MENU
			MenuView = FindViewById<RelativeLayout>(Resource.Id.MenuView);
			Menu = FindViewById<RelativeLayout>(Resource.Id.Menu);

			MenuView.Visibility = ViewStates.Gone;
			MenuView.Clickable = true;

			FatturaMenu = FindViewById<LinearLayout>(Resource.Id.FattureMenu);
			ContrattiMenu = FindViewById<LinearLayout>(Resource.Id.ContrattiMenu);
			DatiMenu = FindViewById<LinearLayout>(Resource.Id.DatiMenu);
            NotificheMenu = FindViewById<LinearLayout>(Resource.Id.NotificheMenu);

			FatturaMenu.Click += delegate {

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

				var intent = new Intent(this, typeof(FatturePage));
				this.StartActivity(intent);

			};

			ContrattiMenu.Click += delegate {

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

				var intent = new Intent(this, typeof(ContrattiPage));
				this.StartActivity(intent);

			};

			DatiMenu.Click += delegate {

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

				var intent = new Intent(this, typeof(InfoDataPage));
				this.StartActivity(intent);

			};

			NotificheMenu.Click += delegate {

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

				var intent = new Intent(this, typeof(NotifichePage));
				this.StartActivity(intent);

			};


            var LogoutMenu = FindViewById<LinearLayout>(Resource.Id.LogoutMenu);
            LogoutMenu.Click += (sender, e) => {
                
                var client = new RestSharp.RestClient(GlobalStructureInstance.Instance.BaseUrl + "/");

                Console.WriteLine(GlobalStructureInstance.Instance.BaseUrl + "/" + "logout");

                var requestN4U = new RestSharp.RestRequest("logout", RestSharp.Method.GET);
                requestN4U.AddHeader("x-access-token", GlobalStructureInstance.Instance.Token);

                client.ExecuteAsync(requestN4U, (arg1, arg2) => {

                    var response = arg1;

                    Console.WriteLine("StatusCode logout:" + response.StatusCode +
                                      "\nContent logout:" + response.Content);


                    RunOnUiThread(() => {

                        var prefEditor = MainActivity.Instance.prefs.Edit();
                        prefEditor.PutString("TokenN4U", "");
                        prefEditor.Commit();

                                    
                        Finish();
                    });
                });

            };
            var LogoutTxtMenu = FindViewById<TextView>(Resource.Id.LogoutTxtMenu);
            LogoutTxtMenu.Typeface = tfCB;

			FatturaTxtMenu = FindViewById<TextView>(Resource.Id.FatturaTxtMenu);
			ContrattiTxtMenu = FindViewById<TextView>(Resource.Id.ContrattiTxtMenu);
			DatiTxtMenu = FindViewById<TextView>(Resource.Id.DatiTxtMenu);
            NotificheTxtMenu = FindViewById<TextView>(Resource.Id.NotificheTxtMenu);

			FatturaTxtMenu.Typeface = tfCB;
			ContrattiTxtMenu.Typeface = tfCB;
			DatiTxtMenu.Typeface = tfCB;
            NotificheTxtMenu.Typeface = tfCB;

			MenuValueAnimator = ValueAnimator.OfInt(0);
			MenuValueAnimator.SetDuration(300);
			MenuValueAnimator.Update += (sender, e) =>
			{

				RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)Menu.LayoutParameters;
				layoutParams.Height = PixelsToDp(4.2f * (int)e.Animation.AnimatedValue);
				layoutParams.Width = PixelsToDp(2 * (int)e.Animation.AnimatedValue);
				Menu.LayoutParameters = layoutParams;

				if (MenuOpen && (int)e.Animation.AnimatedValue == 0)
				{

					MenuOpen = false;

					MenuImg.SetImageResource(Resource.Drawable.MenuIcon);

					MenuView.Visibility = ViewStates.Gone;

				}
				if (!MenuOpen && (int)e.Animation.AnimatedValue == 100)
				{

					MenuOpen = true;

				}
			
			};

			MenuView.Click += delegate
			{

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();
			};

		}

		public void InitNot()
		{

			// GESTIONE NOtifiche
			NotView = FindViewById<RelativeLayout>(Resource.Id.NotificationView);
			ChiudiL = FindViewById<RelativeLayout>(Resource.Id.ChiudiL);
			VediL = FindViewById<RelativeLayout>(Resource.Id.VediL);

			ChiudiTxt = FindViewById<TextView>(Resource.Id.ChiudiTxt);
			VediTxt = FindViewById<TextView>(Resource.Id.VediTxt);
			NotTitTxt = FindViewById<TextView>(Resource.Id.NotificaTit);
			NotTxt = FindViewById<TextView>(Resource.Id.NotificaTxt);

			NotImg = FindViewById<ImageView>(Resource.Id.NotImg);

			ChiudiTxt.Typeface = tfCB;
			VediTxt.Typeface = tfCB;
			NotTitTxt.Typeface = tfCB;
			NotTxt.Typeface = tfCL;

			NotValueAnimator = ValueAnimator.OfInt(0);
			NotValueAnimator.SetDuration(300);
			NotValueAnimator.Update += (sender, e) =>
			{

				RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)NotView.LayoutParameters;
				layoutParams.BottomMargin = - PixelsToDp(60) + PixelsToDp(60 * ((int)e.Animation.AnimatedValue / 100f));
				NotView.LayoutParameters = layoutParams;

				/*
				if (MenuOpen && (int)e.Animation.AnimatedValue == 0)
				{

					MenuOpen = false;

					MenuImg.SetImageResource(Resource.Drawable.MenuIcon);

					MenuView.Visibility = ViewStates.Gone;

				}
				if (!MenuOpen && (int)e.Animation.AnimatedValue == 100)
				{

					MenuOpen = true;

				}*/

			};

			VediL.Click += (sender, e) => {

				if (GlobalStructureInstance.Instance.NotificaObj != null)
				{
					var Not = GlobalStructureInstance.Instance.NotificaObj;

					if (Not.Type == NotificheType.Fattura)
					{
						var intent = new Intent(this, typeof(FatturePage));
						intent.PutExtra("saledocId", Not.Id_Object);
						this.StartActivity(intent);
						Finish();
					}

					if (Not.Type == NotificheType.Contratto)
					{
						var intent = new Intent(this, typeof(ContrattiPage));
						intent.PutExtra("contrattiNumber", Not.Id_Object);
						this.StartActivity(intent);
					}

					if (Not.Type == NotificheType.ScadenzaFattura)
					{
						var intent = new Intent(this, typeof(FatturePage));
						intent.PutExtra("saledocId", Not.Id_Object);
						this.StartActivity(intent);
					}

					GlobalStructureInstance.Instance.NotificaObj = null;

					NotValueAnimator.SetIntValues(100, 0);
					NotValueAnimator.Start();
				}
			};

			ChiudiL.Click += (sender, e) =>
			{

				GlobalStructureInstance.Instance.NotificaObj = null;

				NotValueAnimator.SetIntValues(100, 0);
				NotValueAnimator.Start();

			};
		}

		public void DisplayNot(NotificheObject Not)
		{

			GlobalStructureInstance.Instance.NotificaObj = Not;

			if (Not.Type == NotificheType.Fattura)
			{

				NotImg.SetImageResource(Resource.Drawable.FattureMenu);
				NotTxt.Text = "Nuova Fattura";

			}

			if (Not.Type == NotificheType.Contratto)
			{ 
				NotImg.SetImageResource(Resource.Drawable.ContrattiMenu);
				NotTxt.Text = "Scadenza Contratto";
			}

			if (Not.Type == NotificheType.ScadenzaFattura)
			{ 
				NotImg.SetImageResource(Resource.Drawable.FattureMenu);
				NotTxt.Text = "Scadenza Fattura";
			}



			NotValueAnimator.SetIntValues(0, 100);
			NotValueAnimator.Start();

		}

		private int PixelsToDp(float pixels)
		{
			return (int)Android.Util.TypedValue.ApplyDimension(Android.Util.ComplexUnitType.Dip, pixels, Resources.DisplayMetrics);
		}


        public void SendToken(string token, int time)
        {
            var client = new RestClient(GlobalStructureInstance.Instance.BaseUrl);

            string cookie = GlobalStructureInstance.Instance.Token;

            if (!string.IsNullOrEmpty(cookie) && !string.IsNullOrWhiteSpace(cookie))
            {

                var request = new RestRequest("push", Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("x-access-token", GlobalStructureInstance.Instance.Token);

                JObject oJsonObject = new JObject();

                /*
                "type": "apn",
                "key": "device-key3",
                "token": "token"
                */

                oJsonObject.Add("type", "android");
                oJsonObject.Add("uuid", token);

                request.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);

                //if (!GlobalStructureInstance.Instance.SendToken)
                //{
                    GlobalStructureInstance.Instance.SendToken = true;

                    client.ExecuteAsync(request, (sender, e) =>
                    {

                        IRestResponse response = sender;

                        RunOnUiThread(() =>
                        {
                            Console.WriteLine(response.Server + "\npush StatusCode:" + response.StatusCode + "\nContent:" + response.Content);
                            if (response.StatusCode == System.Net.HttpStatusCode.OK)
                            {

                            }
                            else
                            {
                                GlobalStructureInstance.Instance.SendToken = false;
                                if (time <= 5)
                                    SendToken(token, time + 1);
                            }
                        });
                    });
                //}
            }
            else
            {

                if (time <= 5)
                    SendToken(token, time);

            }

        }
	}
}
