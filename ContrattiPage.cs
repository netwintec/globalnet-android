﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Animation;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Webkit;
using Android.Widget;

namespace GlobalNet
{
	[Activity(Label = "ContrattiPage", ScreenOrientation = ScreenOrientation.Portrait)]
	public class ContrattiPage : Activity
	{

		string contrattiNumber = "";

		Typeface tfCL, tfCB;

		RelativeLayout MenuClick, BackClick;
		ImageView MenuImg;

		LayoutInflater inflater;
		ViewGroup Cont;
		LinearLayout Container;


		List<ContrattiObject> ListContratti;
		ContrattiObject ContrattiObj;
		SedeObject SedeObj;
		ServiziObject ServizioObj;
		ConsumiObject ConsumiObj;

		// CONTRATTI INFO VIEW

		ScrollView ContrattiInfoView;
		LinearLayout ContrattiInfoData;
		LinearLayout ContrattiInfoSedi;
		bool isContrattiInfo = false;

		// SEDE INFO VIEW

		ScrollView SedeInfoView;
		LinearLayout SedeInfoServizi;
		LinearLayout SedeInfoTelefonia;
		bool isSedeInfo = false;

		// MENU VARIABLE 

		RelativeLayout MenuView;
		RelativeLayout Menu; // 200 x 335

		LinearLayout FatturaMenu, ContrattiMenu, DatiMenu, NotificheMenu;
		TextView FatturaTxtMenu, ContrattiTxtMenu, DatiTxtMenu, NotificheTxtMenu;

		ValueAnimator MenuValueAnimator;

		bool MenuOpen = false;

		// NOtification VARIABLE 

		RelativeLayout NotView;
		RelativeLayout ChiudiL, VediL;

		TextView ChiudiTxt, VediTxt, NotTitTxt, NotTxt;
		ImageView NotImg;

		ValueAnimator NotValueAnimator;


		public static ContrattiPage Instance { private set; get; }

		public bool isVisible = true;

		protected override void OnStop()
		{
			base.OnStop();
			isVisible = false;
		}

		protected override void OnResume()
		{
			base.OnResume();

			isVisible = true;


			GlobalStructureInstance.Instance.SetPage(PageName.Contratti);
		}
	
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			Window.RequestFeature(WindowFeatures.NoTitle);

			SetContentView(Resource.Layout.ContrattiPage);

			GlobalStructureInstance.Instance.SetPage(PageName.Contratti);

			ContrattiPage.Instance = this;

			if (Intent.HasExtra("contrattiNumber"))
				contrattiNumber = Intent.GetStringExtra("contrattiNumber");

			tfCL = Typeface.CreateFromAsset(Assets, "fonts/OpenSans_CondLight.ttf");
			tfCB = Typeface.CreateFromAsset(Assets, "fonts/OpenSans_CondBold.ttf");

			//NAVIGATION BAR MENU
			MenuClick = FindViewById<RelativeLayout>(Resource.Id.MenuClick);
			MenuImg = FindViewById<ImageView>(Resource.Id.MenuIcon);

			MenuClick.Click += delegate
			{
				OpenMenu();
			};

			//INFO BAR
			BackClick = FindViewById<RelativeLayout>(Resource.Id.BackClick);
			BackClick.Click += delegate
			{

				if (!isContrattiInfo && !isSedeInfo)
				{
					Finish();
				}

				if (isContrattiInfo && !isSedeInfo)
				{
					isContrattiInfo = false;
					ContrattiInfoView.Visibility = ViewStates.Gone;
				}

				if (isSedeInfo)
				{
					isSedeInfo = false;
					SedeInfoView.Visibility = ViewStates.Gone;
				}

			};

			var DatiTxt = FindViewById<TextView>(Resource.Id.ContrattiTxt);

			DatiTxt.Typeface = tfCB;

			Container = FindViewById<LinearLayout>(Resource.Id.Container);

			inflater = this.LayoutInflater;
			Cont = (ViewGroup)Container;

			var NOContrattiTxt = FindViewById<TextView>(Resource.Id.NOContrattiTxt);

			NOContrattiTxt.Typeface = tfCL;

			ListContratti = GlobalStructureInstance.Instance.listContratti;

			if (ListContratti.Count != 0)
				NOContrattiTxt.Visibility = ViewStates.Gone;

			for (int i = 0; i < ListContratti.Count; i++)
			{

				ContrattiObj = ListContratti[i];

				View InfoDataView = inflater.Inflate(Resource.Layout.ContrattiTemplate, Cont, false);

				var box = InfoDataView.FindViewById<LinearLayout>(Resource.Id.Box);

				var DescrizioneTit = InfoDataView.FindViewById<TextView>(Resource.Id.DescrizioneTit);
				var DescrizioneTxt = InfoDataView.FindViewById<TextView>(Resource.Id.DescrizioneTxt);

				var ContrattoTit = InfoDataView.FindViewById<TextView>(Resource.Id.ContrattoTit);
				var ContrattoTxt = InfoDataView.FindViewById<TextView>(Resource.Id.ContrattoTxt);

				var AttivoTxt = InfoDataView.FindViewById<TextView>(Resource.Id.AttivoTxt);
				var AttivoImage = InfoDataView.FindViewById<RelativeLayout>(Resource.Id.AttivoImage);

				DescrizioneTit.Typeface = tfCL;
				DescrizioneTxt.Typeface = tfCL;

				ContrattoTit.Typeface = tfCL;
				ContrattoTxt.Typeface = tfCL;

				AttivoTxt.Typeface = tfCL;

				DescrizioneTxt.Text = ContrattiObj.descrizione_contratto;
				ContrattoTxt.Text = ContrattiObj.numero_contratto;

				if (ContrattiObj.stato == "attivo")
				{
					AttivoTxt.Text = "Attivo";
					AttivoImage.SetBackgroundResource(Resource.Drawable.GreenCircle);
				}
				else
				{
					AttivoTxt.Text = "Non attivo";
					AttivoImage.SetBackgroundResource(Resource.Drawable.RedCircle);
				}

				int indice = i;
				box.Click += delegate
				{
					InitContrattiInfo(indice);
				};

				Container.AddView(InfoDataView);

			}

			// CONTRATTI INFO

			ContrattiInfoView = FindViewById<ScrollView>(Resource.Id.ContrattoInfoScrollView);
			ContrattiInfoData = FindViewById<LinearLayout>(Resource.Id.ContainerCINFOData);
			ContrattiInfoSedi = FindViewById<LinearLayout>(Resource.Id.ContainerCINFOSedi);

			ContrattiInfoView.Visibility = ViewStates.Gone;


			// SEDE INFO

			SedeInfoView = FindViewById<ScrollView>(Resource.Id.SedeInfoScrollView);
			SedeInfoServizi = FindViewById<LinearLayout>(Resource.Id.ContainerCSEDEServizi);
			SedeInfoTelefonia = FindViewById<LinearLayout>(Resource.Id.ContainerCSEDETelefonia);

			FindViewById<TextView>(Resource.Id.SedeServiziTxt).Typeface = tfCL;
			FindViewById<TextView>(Resource.Id.SedeTelefoniaTxt).Typeface = tfCL;

			FindViewById<TextView>(Resource.Id.NOSedeServiziTxt).Typeface = tfCL;
			FindViewById<TextView>(Resource.Id.NOSedeTelefoniaTxt).Typeface = tfCL;

			SedeInfoView.Visibility = ViewStates.Gone;

			if (!string.IsNullOrEmpty(contrattiNumber))
			{
				int giusto = -1;
				for (int ii = 0; ii < ListContratti.Count; ii++)
				{
					if (ListContratti[ii].numero_contratto == contrattiNumber)
					{
						giusto = ii;
					}
				}

				if (giusto != -1)
					InitContrattiInfo(giusto);

			}

			InitMenu();

            InitNot();

			if (GlobalStructureInstance.Instance.NotificaObj != null)
                DisplayNot(GlobalStructureInstance.Instance.NotificaObj);
		}

		public override void OnBackPressed()
		{
			//base.OnBackPressed();

			if (MenuOpen)
			{

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

			}
			else
			{

				if (!isContrattiInfo && !isSedeInfo)
				{
					Finish();
				}

				if (isContrattiInfo && !isSedeInfo)
				{
					isContrattiInfo = false;
					ContrattiInfoView.Visibility = ViewStates.Gone;
				}

				if (isSedeInfo)
				{
					isSedeInfo = false;
					SedeInfoView.Visibility = ViewStates.Gone;
				}

			}
		}

		public void OpenMenu()
		{

			if (MenuOpen)
			{

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

			}
			else
			{

				MenuView.Visibility = ViewStates.Visible;
				MenuImg.SetImageResource(Resource.Drawable.MenuSpentoIcon);

				MenuValueAnimator.SetIntValues(0, 100);
				MenuValueAnimator.Start();


			}

		}

		public void InitMenu()
		{

			// GESTIONE MENU
			MenuView = FindViewById<RelativeLayout>(Resource.Id.MenuView);
			Menu = FindViewById<RelativeLayout>(Resource.Id.Menu);

			MenuView.Visibility = ViewStates.Gone;
			MenuView.Clickable = true;

			FatturaMenu = FindViewById<LinearLayout>(Resource.Id.FattureMenu);
			ContrattiMenu = FindViewById<LinearLayout>(Resource.Id.ContrattiMenu);
			DatiMenu = FindViewById<LinearLayout>(Resource.Id.DatiMenu);
			NotificheMenu = FindViewById<LinearLayout>(Resource.Id.NotificheMenu);

			FatturaMenu.Click += delegate
			{

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

				var intent = new Intent(this, typeof(FatturePage));
				this.StartActivity(intent);
				Finish();

			};

			ContrattiMenu.Click += delegate
			{

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

			};

			DatiMenu.Click += delegate
			{

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

				var intent = new Intent(this, typeof(InfoDataPage));
				this.StartActivity(intent);
				Finish();

			};

			NotificheMenu.Click += delegate
			{

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

				var intent = new Intent(this, typeof(NotifichePage));
				this.StartActivity(intent);
				Finish();


			};

            var LogoutMenu = FindViewById<LinearLayout>(Resource.Id.LogoutMenu);
            LogoutMenu.Click += (sender, e) => {

                var client = new RestSharp.RestClient(GlobalStructureInstance.Instance.BaseUrl + "/");

                Console.WriteLine(GlobalStructureInstance.Instance.BaseUrl + "/" + "logout");

                var requestN4U = new RestSharp.RestRequest("logout", RestSharp.Method.GET);
                requestN4U.AddHeader("x-access-token", GlobalStructureInstance.Instance.Token);

                client.ExecuteAsync(requestN4U, (arg1, arg2) => {

                    var response = arg1;

                    Console.WriteLine("StatusCode servizi:" + response.StatusCode +
                                                  "\nContent servizi:" + response.Content);


                    RunOnUiThread(() => {

                        var prefEditor = MainActivity.Instance.prefs.Edit();
                        prefEditor.PutString("TokenN4U", "");
                        prefEditor.Commit();

                        //GlobalStructureInstance.Instance.ClearAll();

                        //MainActivity.Instance.LoadView.Visibility = ViewStates.Gone;

                        if(!GlobalStructureInstance.Instance.isHome)
                            HomePage.Instance.Finish();
                        Finish();
                    });
                });

            };
            var LogoutTxtMenu = FindViewById<TextView>(Resource.Id.LogoutTxtMenu);
            LogoutTxtMenu.Typeface = tfCB;

			FatturaTxtMenu = FindViewById<TextView>(Resource.Id.FatturaTxtMenu);
			ContrattiTxtMenu = FindViewById<TextView>(Resource.Id.ContrattiTxtMenu);
			DatiTxtMenu = FindViewById<TextView>(Resource.Id.DatiTxtMenu);
			NotificheTxtMenu = FindViewById<TextView>(Resource.Id.NotificheTxtMenu);

			FatturaTxtMenu.Typeface = tfCB;
			ContrattiTxtMenu.Typeface = tfCB;
			DatiTxtMenu.Typeface = tfCB;
			NotificheTxtMenu.Typeface = tfCB;

			MenuValueAnimator = ValueAnimator.OfInt(0);
			MenuValueAnimator.SetDuration(300);
			MenuValueAnimator.Update += (sender, e) =>
			{

				RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)Menu.LayoutParameters;
				layoutParams.Height = PixelsToDp(4.2f * (int)e.Animation.AnimatedValue);
				layoutParams.Width = PixelsToDp(2 * (int)e.Animation.AnimatedValue);
				Menu.LayoutParameters = layoutParams;

				if (MenuOpen && (int)e.Animation.AnimatedValue == 0)
				{

					MenuOpen = false;

					MenuImg.SetImageResource(Resource.Drawable.MenuIcon);

					MenuView.Visibility = ViewStates.Gone;

				}
				if (!MenuOpen && (int)e.Animation.AnimatedValue == 100)
				{

					MenuOpen = true;

				}

			};

			MenuView.Click += delegate
			{

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

			};

		}

		public void InitContrattiInfo(int indice)
		{

			ContrattiObj = ListContratti[indice];

			ContrattiInfoView.Visibility = ViewStates.Visible;

			ContrattiInfoData.RemoveAllViews();
			ContrattiInfoSedi.RemoveAllViews();

			isContrattiInfo = true;

			for (int i = 0; i < 7; i++)
			{

				View InfoDataView = inflater.Inflate(Resource.Layout.InfoDataTemplate, Cont, false);

				var info = InfoDataView.FindViewById<TextView>(Resource.Id.Info);
				var data = InfoDataView.FindViewById<TextView>(Resource.Id.Data);

				info.Typeface = tfCL;
				data.Typeface = tfCL;

				info.Text = "info " + i;
				data.Text = "data " + i;

				switch (i)
				{

					case 0:
						info.Text = "n° contratto";
						data.Text = ContrattiObj.numero_contratto;
						break;

					case 1:
						info.Text = "descrizione";
						data.Text = ContrattiObj.descrizione_contratto;
						break;

					case 2:
						info.Text = "data sottoscrizione";
                        if (ContrattiObj.DataSottoscrizione.Year > 1900)
                        {
                            data.Text = ContrattiObj.DataSottoscrizione.ToString("dd/MM/yy");
                        }
                        else
                        {
                            data.Text = "N/D";
                        }
						break;

					case 3:
						info.Text = "data attivazione";
                        if (ContrattiObj.DataAttivazione.Year > 1900)
                        {
                            data.Text = ContrattiObj.DataAttivazione.ToString("dd/MM/yy");
                        }
                        else
                        {
                            data.Text = "N/D";
                        }
						break;

					case 4:
						info.Text = "stato";
						data.Text = ContrattiObj.stato;
						break;

					case 5:
						info.Text = "durata";
						data.Text = ContrattiObj.durata + " Mesi";
						break;

					case 6:
						info.Text = "totale";
						data.Text = ContrattiObj.totale + "€";
						break;

				}

				ContrattiInfoData.AddView(InfoDataView);

			}
			for (int i = 0; i < ContrattiObj.ListSede.Count; i++)
			{

				SedeObj = ContrattiObj.ListSede[i];

				View InfoDataView = inflater.Inflate(Resource.Layout.ContrattiSedeTemplate, Cont, false);

				var box = InfoDataView.FindViewById<LinearLayout>(Resource.Id.Box);

				var SedeName = InfoDataView.FindViewById<TextView>(Resource.Id.SedeName);

				SedeName.Typeface = tfCL;
				SedeName.Text = SedeObj.nome_sede;

				int indiceSed = i;
				box.Click += delegate
				{
					InitSedeInfo(indiceSed);
				};

				ContrattiInfoSedi.AddView(InfoDataView);

			}

		}

		public void InitSedeInfo(int indice)
		{

			SedeObj = ContrattiObj.ListSede[indice];

			SedeInfoView.Visibility = ViewStates.Visible;


			SedeInfoServizi.RemoveAllViews();
			SedeInfoTelefonia.RemoveAllViews();

			isSedeInfo = true;

			if (SedeObj.ListServizi.Count != 0)
				FindViewById<TextView>(Resource.Id.NOSedeServiziTxt).Visibility = ViewStates.Gone;
			else
				FindViewById<TextView>(Resource.Id.NOSedeServiziTxt).Visibility = ViewStates.Visible;

			for (int i = 0; i < SedeObj.ListServizi.Count; i++)
			{

				ServizioObj = SedeObj.ListServizi[i];

				View InfoDataView = inflater.Inflate(Resource.Layout.ContrattiSedeServizi, Cont, false);

				var Box = InfoDataView.FindViewById<LinearLayout>(Resource.Id.Box);

				var DescrizioneTxt = InfoDataView.FindViewById<TextView>(Resource.Id.DescrizioneTxt);
				var DescrizioneTit = InfoDataView.FindViewById<TextView>(Resource.Id.DescrizioneTit);

				var AttivoImage = InfoDataView.FindViewById<RelativeLayout>(Resource.Id.AttivoImage);
				var AttivoTxt = InfoDataView.FindViewById<TextView>(Resource.Id.AttivoTxt);

				var QuantitàTit = InfoDataView.FindViewById<TextView>(Resource.Id.QuantitàTit);
				var QuantitàTxt = InfoDataView.FindViewById<TextView>(Resource.Id.QuantitàTxt);

				var CodiceTit = InfoDataView.FindViewById<TextView>(Resource.Id.CodiceTit);
				var CodiceTxt = InfoDataView.FindViewById<TextView>(Resource.Id.CodiceTxt);

				var ImportoTit = InfoDataView.FindViewById<TextView>(Resource.Id.ImportoTit);
				var ImportoTxt = InfoDataView.FindViewById<TextView>(Resource.Id.ImportoTxt);

				var DecorrenzaTit = InfoDataView.FindViewById<TextView>(Resource.Id.DecorrenzaTit);
				var DecorrenzaTxt = InfoDataView.FindViewById<TextView>(Resource.Id.DecorrenzaTxt);

				var DisattivazioneTit = InfoDataView.FindViewById<TextView>(Resource.Id.DisattivazioneTit);
				var DisattivazioneTxt = InfoDataView.FindViewById<TextView>(Resource.Id.DisattivazioneTxt);

				var RicorrenzaTit = InfoDataView.FindViewById<TextView>(Resource.Id.RicorrenzaTit);
				var RicorrenzaTxt = InfoDataView.FindViewById<TextView>(Resource.Id.RicorrenzaTxt);

				DescrizioneTxt.Typeface = tfCL;
				DescrizioneTit.Typeface = tfCL;

				AttivoTxt.Typeface = tfCL;

				QuantitàTit.Typeface = tfCL;
				QuantitàTxt.Typeface = tfCL;

				CodiceTit.Typeface = tfCL;
				CodiceTxt.Typeface = tfCL;

				ImportoTit.Typeface = tfCL;
				ImportoTxt.Typeface = tfCL;

				DecorrenzaTit.Typeface = tfCL;
				DecorrenzaTxt.Typeface = tfCL;

				DisattivazioneTit.Typeface = tfCL;
				DisattivazioneTxt.Typeface = tfCL;

				RicorrenzaTit.Typeface = tfCL;
				RicorrenzaTxt.Typeface = tfCL;

				DescrizioneTxt.Text = ServizioObj.descrizione;
				QuantitàTxt.Text = ServizioObj.quantità;
				CodiceTxt.Text = ServizioObj.codice;
				ImportoTxt.Text = ServizioObj.importo + "€";

                if (ServizioObj.DataDecorrenza.Year > 1900)
                {
                    DecorrenzaTxt.Text = ServizioObj.DataDecorrenza.ToString("dd/MM/yy");
                }
                else
                {
                    DecorrenzaTxt.Text = "N/D";
                }

                if (ServizioObj.DataDisattivazione.Year > 1900)
                {
                    DisattivazioneTxt.Text = ServizioObj.DataDisattivazione.ToString("dd/MM/yy");
                }
                else
                {
                    DisattivazioneTxt.Text = "N/D";
                }

				RicorrenzaTxt.Text = ServizioObj.ricorrenza;

				if (ServizioObj.stato_contratti_servizi == "attivo")
				{
					AttivoTxt.Text = "Attivo";
					AttivoImage.SetBackgroundResource(Resource.Drawable.GreenCircle);
				}
				else
				{
					AttivoTxt.Text = "Non attivo";
					AttivoImage.SetBackgroundResource(Resource.Drawable.RedCircle);
				}

				SedeInfoServizi.AddView(InfoDataView);
			}


			if (SedeObj.ListConsumi.Count != 0)
				FindViewById<TextView>(Resource.Id.NOSedeTelefoniaTxt).Visibility = ViewStates.Gone;
			else
				FindViewById<TextView>(Resource.Id.NOSedeTelefoniaTxt).Visibility = ViewStates.Visible;

			for (int i = 0; i < SedeObj.ListConsumi.Count; i++)
			{
				ConsumiObj = SedeObj.ListConsumi[i];

				View InfoDataView = inflater.Inflate(Resource.Layout.ContrattiSedeTelefonia, Cont, false);

				var Box = InfoDataView.FindViewById<LinearLayout>(Resource.Id.Box);

				var ClienteTit = InfoDataView.FindViewById<TextView>(Resource.Id.ClienteTit);
				var ClienteTxt = InfoDataView.FindViewById<TextView>(Resource.Id.ClienteTxt);

				var AttivoImage = InfoDataView.FindViewById<RelativeLayout>(Resource.Id.AttivoImage);
				var AttivoTxt = InfoDataView.FindViewById<TextView>(Resource.Id.AttivoTxt);

				var PTTit = InfoDataView.FindViewById<TextView>(Resource.Id.PTTit);
				var PTTxt = InfoDataView.FindViewById<TextView>(Resource.Id.PTTxt);

				var TSTit = InfoDataView.FindViewById<TextView>(Resource.Id.TSTit);
				var TSTxt = InfoDataView.FindViewById<TextView>(Resource.Id.TSTxt);

				var DATit = InfoDataView.FindViewById<TextView>(Resource.Id.DATit);
				var DATxt = InfoDataView.FindViewById<TextView>(Resource.Id.DATxt);

				var DDTit = InfoDataView.FindViewById<TextView>(Resource.Id.DDTit);
				var DDTxt = InfoDataView.FindViewById<TextView>(Resource.Id.DDTxt);

				ClienteTit.Typeface = tfCL;
				ClienteTxt.Typeface = tfCL;

				AttivoTxt.Typeface = tfCL;

				PTTit.Typeface = tfCL;
				PTTxt.Typeface = tfCL;

				TSTit.Typeface = tfCL;
				TSTxt.Typeface = tfCL;

				DATit.Typeface = tfCL;
				DATxt.Typeface = tfCL;

				DDTit.Typeface = tfCL;
				DDTxt.Typeface = tfCL;

				ClienteTxt.Text = ConsumiObj.cli;
				PTTxt.Text = ConsumiObj.piano_tariffario;
				TSTxt.Text = ConsumiObj.tipologia_servizio;

                if (ConsumiObj.DataAttivazione.Year > 1900)
                {
                    DATxt.Text = ConsumiObj.DataAttivazione.ToString("dd/MM/yy");
                }else
                {
                    DATxt.Text = "N/D";
                }

                if (ConsumiObj.DataDisattivazione.Year > 1900)
                {
                    DDTxt.Text = ConsumiObj.DataDisattivazione.ToString("dd/MM/yy");
                }
                else
                {
                    DDTxt.Text = "N/D";
                }
				

				if (ConsumiObj.stato_contratti_servizi == "attivo")
				{
					AttivoTxt.Text = "Attivo";
					AttivoImage.SetBackgroundResource(Resource.Drawable.GreenCircle);
				}
				else
				{
					AttivoTxt.Text = "Non attivo";
					AttivoImage.SetBackgroundResource(Resource.Drawable.RedCircle);
				}

				SedeInfoTelefonia.AddView(InfoDataView);
			}

		}

		private int PixelsToDp(float pixels)
		{
			return (int)Android.Util.TypedValue.ApplyDimension(Android.Util.ComplexUnitType.Dip, pixels, Resources.DisplayMetrics);
		}


		public void InitNot()
		{

			// GESTIONE NOtifiche
			NotView = FindViewById<RelativeLayout>(Resource.Id.NotificationView);
			ChiudiL = FindViewById<RelativeLayout>(Resource.Id.ChiudiL);
			VediL = FindViewById<RelativeLayout>(Resource.Id.VediL);

			ChiudiTxt = FindViewById<TextView>(Resource.Id.ChiudiTxt);
			VediTxt = FindViewById<TextView>(Resource.Id.VediTxt);
			NotTitTxt = FindViewById<TextView>(Resource.Id.NotificaTit);
			NotTxt = FindViewById<TextView>(Resource.Id.NotificaTxt);

			NotImg = FindViewById<ImageView>(Resource.Id.NotImg);

			ChiudiTxt.Typeface = tfCB;
			VediTxt.Typeface = tfCB;
			NotTitTxt.Typeface = tfCB;
			NotTxt.Typeface = tfCL;

			NotValueAnimator = ValueAnimator.OfInt(0);
			NotValueAnimator.SetDuration(300);
			NotValueAnimator.Update += (sender, e) =>
			{

				RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)NotView.LayoutParameters;
				layoutParams.BottomMargin = -PixelsToDp(60) + PixelsToDp(60 * ((int)e.Animation.AnimatedValue / 100f));
				NotView.LayoutParameters = layoutParams;

			};

			VediL.Click += (sender, e) =>
			{

				if (GlobalStructureInstance.Instance.NotificaObj != null)
				{
					var Not = GlobalStructureInstance.Instance.NotificaObj;

					if (Not.Type == NotificheType.Fattura)
					{
						var intent = new Intent(this, typeof(FatturePage));
						intent.PutExtra("saledocId", Not.Id_Object);
						this.StartActivity(intent);
						Finish();
					}

					if (Not.Type == NotificheType.Contratto)
					{
						int giusto = -1;
						for (int ii = 0; ii < ListContratti.Count; ii++)
						{
							if (ListContratti[ii].numero_contratto == Not.Id_Object)
							{
								giusto = ii;
							}
						}

						if (giusto != -1)
						{
							if (SedeInfoView.Visibility != ViewStates.Gone)
								SedeInfoView.Visibility = ViewStates.Gone;
							
							InitContrattiInfo(giusto);
						}

						NotValueAnimator.SetIntValues(100, 0);
						NotValueAnimator.Start();
					}

					if (Not.Type == NotificheType.ScadenzaFattura)
					{
						var intent = new Intent(this, typeof(FatturePage));
						intent.PutExtra("saledocId", Not.Id_Object);
						this.StartActivity(intent);
						Finish();
					}
				
					/*if (Not.Type == NotificheType.Contratto)
					{
						var intent = new Intent(this, typeof(ContrattiPage));
						intent.PutExtra("contrattiNumber", Not.Id_Object);
						this.StartActivity(intent);
						Finish();
					}

					if (Not.Type == NotificheType.ScadenzaFattura)
					{
						if (!string.IsNullOrEmpty(Not.Id_Object))
						{
							//PdfView.Visibility = ViewStates.Visible;
							//Pdf.Visibility = ViewStates.Gone;
							//Pdf.LoadUrl("http://genio.globalnetitalia.it/ext_documento.aspx?saledocId=" + Not.Id_Object);
							//isPdfOpen = true;
						}
					}*/

					GlobalStructureInstance.Instance.NotificaObj = null;
				}
			};

			ChiudiL.Click += (sender, e) =>
			{

				GlobalStructureInstance.Instance.NotificaObj = null;

				NotValueAnimator.SetIntValues(100, 0);
				NotValueAnimator.Start();

			};

		}

		public void DisplayNot(NotificheObject Not)
		{

			GlobalStructureInstance.Instance.NotificaObj = Not;

			if (Not.Type == NotificheType.Fattura)
			{

				NotImg.SetImageResource(Resource.Drawable.FattureMenu);
				NotTxt.Text = "Nuova Fattura";

				NotValueAnimator.SetIntValues(0, 100);
				NotValueAnimator.Start();
			}

			if (Not.Type == NotificheType.Contratto)
			{
				NotImg.SetImageResource(Resource.Drawable.ContrattiMenu);
				NotTxt.Text = "Scadenza Contratto";

				NotValueAnimator.SetIntValues(0, 100);
				NotValueAnimator.Start();
			}

			if (Not.Type == NotificheType.ScadenzaFattura)
			{
				NotImg.SetImageResource(Resource.Drawable.FattureMenu);
				NotTxt.Text = "Scadenza Fattura";

				NotValueAnimator.SetIntValues(0, 100);
				NotValueAnimator.Start();

			}


		}

		public void AddContratto(NotificheObject Not)
		{


		}
	

	}
}
