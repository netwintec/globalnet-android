﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Animation;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace GlobalNet
{
	[Activity(Label = "InfoDataPage", ScreenOrientation = ScreenOrientation.Portrait)]
	public class InfoDataPage : Activity
	{

		Typeface tfCL, tfCB;

		RelativeLayout MenuClick, BackClick;
		ImageView MenuImg;

		LayoutInflater inflater;
		ViewGroup Cont;
		LinearLayout Container;

		// MENU VARIABLE 

		RelativeLayout MenuView;
		RelativeLayout Menu; // 200 x 335

		LinearLayout FatturaMenu, ContrattiMenu, DatiMenu, NotificheMenu;
		TextView FatturaTxtMenu, ContrattiTxtMenu, DatiTxtMenu, NotificheTxtMenu;

		ValueAnimator MenuValueAnimator;

		bool MenuOpen = false;

		AnagraficaObject ao;

		// NOtification VARIABLE 

		RelativeLayout NotView;
		RelativeLayout ChiudiL, VediL;

		TextView ChiudiTxt, VediTxt, NotTitTxt, NotTxt;
		ImageView NotImg;

		ValueAnimator NotValueAnimator;

		public static InfoDataPage Instance { private set; get; }

		public bool isVisible = true;

		protected override void OnStop()
		{
			base.OnStop();
			isVisible = false;
		}

		protected override void OnResume()
		{
			base.OnResume();

			isVisible = true;

			GlobalStructureInstance.Instance.SetPage(PageName.Dati);
		}
	
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			Window.RequestFeature(WindowFeatures.NoTitle);

			SetContentView(Resource.Layout.InfoDataPage);

			InfoDataPage.Instance = this;
			GlobalStructureInstance.Instance.SetPage(PageName.Dati);

			tfCL = Typeface.CreateFromAsset(Assets, "fonts/OpenSans_CondLight.ttf");
			tfCB = Typeface.CreateFromAsset(Assets, "fonts/OpenSans_CondBold.ttf");

			//NAVIGATION BAR MENU
			MenuClick = FindViewById<RelativeLayout>(Resource.Id.MenuClick);
			MenuImg = FindViewById<ImageView>(Resource.Id.MenuIcon);

			MenuClick.Click += delegate
			{
				OpenMenu();
			};

			//INFO BAR
			BackClick = FindViewById<RelativeLayout>(Resource.Id.BackClick);
			BackClick.Click += delegate
			{
				Finish();
			};

			var DatiTxt = FindViewById<TextView>(Resource.Id.DatiTxt);

			DatiTxt.Typeface = tfCB;

			Container = FindViewById<LinearLayout>(Resource.Id.Container);

			inflater = this.LayoutInflater;
			Cont = (ViewGroup)Container;

			if (GlobalStructureInstance.Instance.listAnagrafica.Count == 0)
			{
				Finish();
				return;
			}
			else
			{
				ao = GlobalStructureInstance.Instance.listAnagrafica[0];
			}

			for (int i = 0; i < 9; i++)
			{

				View InfoDataView = inflater.Inflate(Resource.Layout.InfoDataTemplate, Cont, false);

				var info = InfoDataView.FindViewById<TextView>(Resource.Id.Info);
				var data = InfoDataView.FindViewById<TextView>(Resource.Id.Data);

				info.Typeface = tfCL;
				data.Typeface = tfCL;

				info.Text = "info " + i;
				data.Text = "data " + i;

				switch (i)
				{

					case 0:
						info.Text = "ragione sociale";
						data.Text = ao.RagioneSociale;
						break;

					case 1:
						info.Text = "codice cliente";
						data.Text = ao.CodiceCliente;
						break;

					case 2:
						info.Text = "indirizzo";
						data.Text = ao.Indirizzo;
						break;

					case 3:
						info.Text = "contatti";

						string cont =
							"tel:" + ao.telefono + "\n" +
							"fax:" + ao.fax + "\n" +
							"email:" + ao.email;

						data.Text = cont;
						break;

					case 4:
						info.Text = "codice fiscale";
						data.Text = ao.CodiceFiscale;
						break;

					case 5:
						info.Text = "P.IVA";
						data.Text = ao.PartitaIva;
						break;

					case 6:
						info.Text = "condizione pagamento";
						data.Text = ao.condizioni_pagamento;
						break;

					case 7:
						info.Text = "IBAN";
						data.Text = ao.iban;
						break;

					case 8:
						info.Text = "commerciale";

						string comm =
						ao.Commerciale + "\n" +
						"cell:" + ao.Cellulare + "\n" +
						"email:" + ao.EmailCommerciale;

						data.Text = comm;
						break;

				}

				Container.AddView(InfoDataView);

			}

			InitMenu();

			InitNot();

			if (GlobalStructureInstance.Instance.NotificaObj != null)
				DisplayNot(GlobalStructureInstance.Instance.NotificaObj);
		}

		public override void OnBackPressed()
		{
			//base.OnBackPressed();

			if (MenuOpen)
			{

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

			}
			else
			{
				Finish();
			}
		}

		public void OpenMenu()
		{

			if (MenuOpen)
			{

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

			}
			else
			{

				MenuView.Visibility = ViewStates.Visible;
				MenuImg.SetImageResource(Resource.Drawable.MenuSpentoIcon);

				MenuValueAnimator.SetIntValues(0, 100);
				MenuValueAnimator.Start();


			}

		}

		public void InitMenu()
		{

			// GESTIONE MENU
			MenuView = FindViewById<RelativeLayout>(Resource.Id.MenuView);
			Menu = FindViewById<RelativeLayout>(Resource.Id.Menu);

			MenuView.Visibility = ViewStates.Gone;
			MenuView.Clickable = true;

			FatturaMenu = FindViewById<LinearLayout>(Resource.Id.FattureMenu);
			ContrattiMenu = FindViewById<LinearLayout>(Resource.Id.ContrattiMenu);
			DatiMenu = FindViewById<LinearLayout>(Resource.Id.DatiMenu);
			NotificheMenu = FindViewById<LinearLayout>(Resource.Id.NotificheMenu);

			FatturaMenu.Click += delegate
			{

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

				var intent = new Intent(this, typeof(FatturePage));
				this.StartActivity(intent);
				Finish();

			};

			ContrattiMenu.Click += delegate
			{

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

				var intent = new Intent(this, typeof(ContrattiPage));
				this.StartActivity(intent);
				Finish();

			};

			DatiMenu.Click += delegate
			{

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

			};

			NotificheMenu.Click += delegate
			{

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

				var intent = new Intent(this, typeof(NotifichePage));
				this.StartActivity(intent);
				Finish();

			};

            var LogoutMenu = FindViewById<LinearLayout>(Resource.Id.LogoutMenu);
            LogoutMenu.Click += (sender, e) => {

                var client = new RestSharp.RestClient(GlobalStructureInstance.Instance.BaseUrl + "/");

                Console.WriteLine(GlobalStructureInstance.Instance.BaseUrl + "/" + "logout");

                var requestN4U = new RestSharp.RestRequest("logout", RestSharp.Method.GET);
                requestN4U.AddHeader("x-access-token", GlobalStructureInstance.Instance.Token);

                client.ExecuteAsync(requestN4U, (arg1, arg2) => {

                    var response = arg1;

                    Console.WriteLine("StatusCode servizi:" + response.StatusCode +
                                                  "\nContent servizi:" + response.Content);


                    RunOnUiThread(() => {

                        var prefEditor = MainActivity.Instance.prefs.Edit();
                        prefEditor.PutString("TokenN4U", "");
                        prefEditor.Commit();

                        //GlobalStructureInstance.Instance.ClearAll();

                        //MainActivity.Instance.LoadView.Visibility = ViewStates.Gone;

                        HomePage.Instance.Finish();
                        Finish();
                    });
                });

            };
            var LogoutTxtMenu = FindViewById<TextView>(Resource.Id.LogoutTxtMenu);
            LogoutTxtMenu.Typeface = tfCB;

			FatturaTxtMenu = FindViewById<TextView>(Resource.Id.FatturaTxtMenu);
			ContrattiTxtMenu = FindViewById<TextView>(Resource.Id.ContrattiTxtMenu);
			DatiTxtMenu = FindViewById<TextView>(Resource.Id.DatiTxtMenu);
			NotificheTxtMenu = FindViewById<TextView>(Resource.Id.NotificheTxtMenu);

			FatturaTxtMenu.Typeface = tfCB;
			ContrattiTxtMenu.Typeface = tfCB;
			DatiTxtMenu.Typeface = tfCB;
			NotificheTxtMenu.Typeface = tfCB;

			MenuValueAnimator = ValueAnimator.OfInt(0);
			MenuValueAnimator.SetDuration(300);
			MenuValueAnimator.Update += (sender, e) =>
			{

				RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)Menu.LayoutParameters;
                layoutParams.Height = PixelsToDp(4.2f * (int)e.Animation.AnimatedValue);
				layoutParams.Width = PixelsToDp(2 * (int)e.Animation.AnimatedValue);
				Menu.LayoutParameters = layoutParams;

				if (MenuOpen && (int)e.Animation.AnimatedValue == 0)
				{

					MenuOpen = false;

					MenuImg.SetImageResource(Resource.Drawable.MenuIcon);

					MenuView.Visibility = ViewStates.Gone;

				}
				if (!MenuOpen && (int)e.Animation.AnimatedValue == 100)
				{

					MenuOpen = true;

				}

			};

			MenuView.Click += delegate
			{

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

			};

		}

		public void InitNot()
		{

			// GESTIONE NOtifiche
			NotView = FindViewById<RelativeLayout>(Resource.Id.NotificationView);
			ChiudiL = FindViewById<RelativeLayout>(Resource.Id.ChiudiL);
			VediL = FindViewById<RelativeLayout>(Resource.Id.VediL);

			ChiudiTxt = FindViewById<TextView>(Resource.Id.ChiudiTxt);
			VediTxt = FindViewById<TextView>(Resource.Id.VediTxt);
			NotTitTxt = FindViewById<TextView>(Resource.Id.NotificaTit);
			NotTxt = FindViewById<TextView>(Resource.Id.NotificaTxt);

			NotImg = FindViewById<ImageView>(Resource.Id.NotImg);

			ChiudiTxt.Typeface = tfCB;
			VediTxt.Typeface = tfCB;
			NotTitTxt.Typeface = tfCB;
			NotTxt.Typeface = tfCL;

			NotValueAnimator = ValueAnimator.OfInt(0);
			NotValueAnimator.SetDuration(300);
			NotValueAnimator.Update += (sender, e) =>
			{

				RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)NotView.LayoutParameters;
				layoutParams.BottomMargin = -PixelsToDp(60) + PixelsToDp(60 * ((int)e.Animation.AnimatedValue / 100f));
				NotView.LayoutParameters = layoutParams;

				/*
				if (MenuOpen && (int)e.Animation.AnimatedValue == 0)
				{

					MenuOpen = false;

					MenuImg.SetImageResource(Resource.Drawable.MenuIcon);

					MenuView.Visibility = ViewStates.Gone;

				}
				if (!MenuOpen && (int)e.Animation.AnimatedValue == 100)
				{

					MenuOpen = true;

				}*/

			};

			VediL.Click += (sender, e) =>
			{

				if (GlobalStructureInstance.Instance.NotificaObj != null)
				{
					var Not = GlobalStructureInstance.Instance.NotificaObj;

					if (Not.Type == NotificheType.Fattura)
					{
						var intent = new Intent(this, typeof(FatturePage));
						intent.PutExtra("saledocId", Not.Id_Object);
						this.StartActivity(intent);
						Finish();
					}

					if (Not.Type == NotificheType.Contratto)
					{
						var intent = new Intent(this, typeof(ContrattiPage));
						intent.PutExtra("contrattiNumber", Not.Id_Object);
						this.StartActivity(intent);
						Finish();
					}

					if (Not.Type == NotificheType.ScadenzaFattura)
					{
						var intent = new Intent(this, typeof(FatturePage));
						intent.PutExtra("saledocId", Not.Id_Object);
						this.StartActivity(intent);
						Finish();
					}
				
					GlobalStructureInstance.Instance.NotificaObj = null;

				}
			};
		
			ChiudiL.Click += (sender, e) =>
			{

				GlobalStructureInstance.Instance.NotificaObj = null;

				NotValueAnimator.SetIntValues(100, 0);
				NotValueAnimator.Start();

			};

		}

		public void DisplayNot(NotificheObject Not)
		{

			GlobalStructureInstance.Instance.NotificaObj = Not;

			if (Not.Type == NotificheType.Fattura)
			{

				NotImg.SetImageResource(Resource.Drawable.FattureMenu);
				NotTxt.Text = "Nuova Fattura";

			}

			if (Not.Type == NotificheType.Contratto)
			{
				NotImg.SetImageResource(Resource.Drawable.ContrattiMenu);
				NotTxt.Text = "Scadenza Contratto";
			}

			if (Not.Type == NotificheType.ScadenzaFattura)
			{
				NotImg.SetImageResource(Resource.Drawable.FattureMenu);
				NotTxt.Text = "Scadenza Fattura";

			}

			NotValueAnimator.SetIntValues(0, 100);
			NotValueAnimator.Start();
		}
	

		private int PixelsToDp(float pixels)
		{
			return (int)Android.Util.TypedValue.ApplyDimension(Android.Util.ComplexUnitType.Dip, pixels, Resources.DisplayMetrics);
		}


	}
}
