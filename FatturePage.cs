﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Animation;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Webkit;
using Android.Widget;

namespace GlobalNet
{
	[Activity(Label = "FatturePage", ScreenOrientation = ScreenOrientation.Portrait)]
	public class FatturePage : Activity
	{
		string saledocId = "";

		Typeface tfCL, tfCB;

		RelativeLayout MenuClick, BackClick;
		ImageView MenuImg;

		LayoutInflater inflater;
		ViewGroup Cont;
		LinearLayout Container;

		List<DocumentiObject> ListDocumenti;
		DocumentiObject DocumentiObj;

		// PDF VIEW
		RelativeLayout PdfView;
		WebView Pdf;
		ProgressBar loading;
		bool isPdfOpen = false;

		// MENU VARIABLE 

		RelativeLayout MenuView;
		RelativeLayout Menu; // 200 x 335

		LinearLayout FatturaMenu, ContrattiMenu, DatiMenu, NotificheMenu;
		TextView FatturaTxtMenu, ContrattiTxtMenu, DatiTxtMenu, NotificheTxtMenu;

		ValueAnimator MenuValueAnimator;

		bool MenuOpen = false;

		// NOtification VARIABLE 

		RelativeLayout NotView;
		RelativeLayout ChiudiL, VediL;

		TextView ChiudiTxt, VediTxt, NotTitTxt, NotTxt;
		ImageView NotImg;

		ValueAnimator NotValueAnimator;

		public static FatturePage Instance { private set; get; }

		public bool isVisible = true;

		protected override void OnStop()
		{
			base.OnStop();
			isVisible = false;
		}

		protected override void OnResume()
		{
			base.OnResume();

			isVisible = true;

			GlobalStructureInstance.Instance.SetPage(PageName.Fatture);
		}
	
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			Window.RequestFeature(WindowFeatures.NoTitle);

			SetContentView(Resource.Layout.FatturePage);

			GlobalStructureInstance.Instance.SetPage(PageName.Fatture);

			FatturePage.Instance = this;

			if (Intent.HasExtra("saledocId"))
				saledocId = Intent.GetStringExtra("saledocId");

			tfCL = Typeface.CreateFromAsset(Assets, "fonts/OpenSans_CondLight.ttf");
			tfCB = Typeface.CreateFromAsset(Assets, "fonts/OpenSans_CondBold.ttf");

			//NAVIGATION BAR MENU
			MenuClick = FindViewById<RelativeLayout>(Resource.Id.MenuClick);
			MenuImg = FindViewById<ImageView>(Resource.Id.MenuIcon);

			MenuClick.Click += delegate
			{
				OpenMenu();
			};

			//INFO BAR
			BackClick = FindViewById<RelativeLayout>(Resource.Id.BackClick);
			BackClick.Click += delegate
			{
				if (isPdfOpen)
				{
					PdfView.Visibility = ViewStates.Gone;
					isPdfOpen = false;
					Pdf.ClearFormData();
				}
				else
					Finish();

			};

			var DatiTxt = FindViewById<TextView>(Resource.Id.FattureTxt);

			DatiTxt.Typeface = tfCB;

			Container = FindViewById<LinearLayout>(Resource.Id.Container);

			inflater = this.LayoutInflater;
			Cont = (ViewGroup)Container;

			var NOFattureTxt = FindViewById<TextView>(Resource.Id.NOFattureTxt);

			NOFattureTxt.Typeface = tfCL;

			ListDocumenti = GlobalStructureInstance.Instance.listDocumenti;

			if (ListDocumenti.Count != 0)
				NOFattureTxt.Visibility = ViewStates.Gone;

			for (int i = 0; i < ListDocumenti.Count; i++)
			{

				DocumentiObj = ListDocumenti[i];

				View InfoDataView = inflater.Inflate(Resource.Layout.FattureTemplate, Cont, false);

				var box = InfoDataView.FindViewById<LinearLayout>(Resource.Id.Box);

				var dataTit = InfoDataView.FindViewById<TextView>(Resource.Id.DataTit);
				var dataTxt = InfoDataView.FindViewById<TextView>(Resource.Id.DataTxt);

				var fatturaTit = InfoDataView.FindViewById<TextView>(Resource.Id.FatturaTit);
				var fatturaTxt = InfoDataView.FindViewById<TextView>(Resource.Id.FatturaTxt);

				var TotaleTit = InfoDataView.FindViewById<TextView>(Resource.Id.TotaleTit);
				var TotaleTxt = InfoDataView.FindViewById<TextView>(Resource.Id.TotaleTxt);

				var newData = InfoDataView.FindViewById<RelativeLayout>(Resource.Id.NewData);

				if (GlobalStructureInstance.Instance.NotificaObj != null)
				{
					var Not = GlobalStructureInstance.Instance.NotificaObj;

					if (Not.Type == NotificheType.Fattura)
					{
						if (DocumentiObj.numero_documento != Not.Fattura.numero_documento)
							newData.Visibility = ViewStates.Gone;
						else
							GlobalStructureInstance.Instance.NotificaObj = null;
					}
					else
					{
						newData.Visibility = ViewStates.Gone;
					}
				}
				else
				{
					newData.Visibility = ViewStates.Gone;
				}

				dataTit.Typeface = tfCL;
				dataTxt.Typeface = tfCL;

				fatturaTit.Typeface = tfCL;
				fatturaTxt.Typeface = tfCL;

				TotaleTit.Typeface = tfCL;
				TotaleTxt.Typeface = tfCL;

				//info.Text = "info " + i;
				//data.Text = "data " + i;
				dataTxt.Text = DocumentiObj.DataDocumento.ToString("dd/MM/yy");
				fatturaTxt.Text = DocumentiObj.numero_documento;
                TotaleTxt.Text = DocumentiObj.totale_ivato.ToString("F2")+"€";

				int indice = i;
				box.Click += delegate
				{
					PdfView.Visibility = ViewStates.Visible;
					Pdf.Visibility = ViewStates.Gone;
					Pdf.LoadUrl("http://genio.globalnetitalia.it/ext_documento.aspx?saledocId=" + ListDocumenti[indice].SaleDocId);
					isPdfOpen = true;
				};

				Container.AddView(InfoDataView);

			}

			PdfView = FindViewById<RelativeLayout>(Resource.Id.PdfView);
			Pdf = FindViewById<WebView>(Resource.Id.PdfWeb);
			loading = FindViewById<ProgressBar>(Resource.Id.Loading);

			PdfView.Visibility = ViewStates.Gone;

			Pdf.Settings.JavaScriptEnabled = true;
			Pdf.Settings.PluginsEnabled = true;
			//Pdf.LoadUrl("https://docs.google.com/gview?embedded=true&url="+MainActivity.Instance.ListManuale[0].url);

			Pdf.SetWebViewClient(new WebViewClient());
			Pdf.SetWebChromeClient(new MyWebViewChromeHome());

			loading.IndeterminateDrawable.SetColorFilter(Color.Rgb(28, 151, 219), PorterDuff.Mode.Multiply);

			if (!string.IsNullOrEmpty(saledocId))
			{
				PdfView.Visibility = ViewStates.Visible;
				Pdf.Visibility = ViewStates.Gone;
				Pdf.LoadUrl("http://genio.globalnetitalia.it/ext_documento.aspx?saledocId=" + saledocId);
				isPdfOpen = true;
			}


			InitMenu();

            InitNot();

			if (GlobalStructureInstance.Instance.NotificaObj != null)
			{

				var Not = GlobalStructureInstance.Instance.NotificaObj;

				if (Not.Type == NotificheType.Contratto)
				{
                    DisplayNot(GlobalStructureInstance.Instance.NotificaObj);
				}

				if (Not.Type == NotificheType.ScadenzaFattura)
				{
                    DisplayNot(GlobalStructureInstance.Instance.NotificaObj);

				}



			}
		}

		public override void OnBackPressed()
		{
			//base.OnBackPressed();

			if (MenuOpen)
			{

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

			}
			else
			{

				if (isPdfOpen)
				{
					PdfView.Visibility = ViewStates.Gone;
					isPdfOpen = false;
					Pdf.ClearFormData();
				}
				else
					Finish();

			}
		}

		public void OpenMenu()
		{

			if (MenuOpen)
			{

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

			}
			else
			{

				MenuView.Visibility = ViewStates.Visible;
				MenuImg.SetImageResource(Resource.Drawable.MenuSpentoIcon);

				MenuValueAnimator.SetIntValues(0, 100);
				MenuValueAnimator.Start();


			}

		}

		public void InitMenu()
		{

			// GESTIONE MENU
			MenuView = FindViewById<RelativeLayout>(Resource.Id.MenuView);
			Menu = FindViewById<RelativeLayout>(Resource.Id.Menu);

			MenuView.Visibility = ViewStates.Gone;
			MenuView.Clickable = true;

			FatturaMenu = FindViewById<LinearLayout>(Resource.Id.FattureMenu);
			ContrattiMenu = FindViewById<LinearLayout>(Resource.Id.ContrattiMenu);
			DatiMenu = FindViewById<LinearLayout>(Resource.Id.DatiMenu);
			NotificheMenu = FindViewById<LinearLayout>(Resource.Id.NotificheMenu);

			FatturaMenu.Click += delegate
			{

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

			};

			ContrattiMenu.Click += delegate
			{

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

				var intent = new Intent(this, typeof(ContrattiPage));
				this.StartActivity(intent);
				Finish();

			};

			DatiMenu.Click += delegate
			{

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

				var intent = new Intent(this, typeof(InfoDataPage));
				this.StartActivity(intent);
				Finish();

			};

			NotificheMenu.Click += delegate
			{

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

				var intent = new Intent(this, typeof(NotifichePage));
				this.StartActivity(intent);
				Finish();


			};

            var LogoutMenu = FindViewById<LinearLayout>(Resource.Id.LogoutMenu);
            LogoutMenu.Click += (sender, e) => {

                var client = new RestSharp.RestClient(GlobalStructureInstance.Instance.BaseUrl + "/");

                Console.WriteLine(GlobalStructureInstance.Instance.BaseUrl + "/" + "logout");

                var requestN4U = new RestSharp.RestRequest("logout", RestSharp.Method.GET);
                requestN4U.AddHeader("x-access-token", GlobalStructureInstance.Instance.Token);

                client.ExecuteAsync(requestN4U, (arg1, arg2) => {

                    var response = arg1;

                    Console.WriteLine("StatusCode servizi:" + response.StatusCode +
                                                  "\nContent servizi:" + response.Content);


                    RunOnUiThread(() => {

                        var prefEditor = MainActivity.Instance.prefs.Edit();
                        prefEditor.PutString("TokenN4U", "");
                        prefEditor.Commit();

                        //GlobalStructureInstance.Instance.ClearAll();

                        // MainActivity.Instance.LoadView.Visibility = ViewStates.Gone;

                        HomePage.Instance.Finish();
                        Finish();
                    });
                });

            };
            var LogoutTxtMenu = FindViewById<TextView>(Resource.Id.LogoutTxtMenu);
            LogoutTxtMenu.Typeface = tfCB;

			FatturaTxtMenu = FindViewById<TextView>(Resource.Id.FatturaTxtMenu);
			ContrattiTxtMenu = FindViewById<TextView>(Resource.Id.ContrattiTxtMenu);
			DatiTxtMenu = FindViewById<TextView>(Resource.Id.DatiTxtMenu);
			NotificheTxtMenu = FindViewById<TextView>(Resource.Id.NotificheTxtMenu);

			FatturaTxtMenu.Typeface = tfCB;
			ContrattiTxtMenu.Typeface = tfCB;
			DatiTxtMenu.Typeface = tfCB;
			NotificheTxtMenu.Typeface = tfCB;

			MenuValueAnimator = ValueAnimator.OfInt(0);
			MenuValueAnimator.SetDuration(300);
			MenuValueAnimator.Update += (sender, e) =>
			{

				RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)Menu.LayoutParameters;
                layoutParams.Height = PixelsToDp(4.2f * (int)e.Animation.AnimatedValue);
				layoutParams.Width = PixelsToDp(2 * (int)e.Animation.AnimatedValue);
				Menu.LayoutParameters = layoutParams;

				if (MenuOpen && (int)e.Animation.AnimatedValue == 0)
				{

					MenuOpen = false;

					MenuImg.SetImageResource(Resource.Drawable.MenuIcon);

					MenuView.Visibility = ViewStates.Gone;

				}
				if (!MenuOpen && (int)e.Animation.AnimatedValue == 100)
				{

					MenuOpen = true;

				}

			};

			MenuView.Click += delegate
			{

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

			};

		}

		private int PixelsToDp(float pixels)
		{
			return (int)Android.Util.TypedValue.ApplyDimension(Android.Util.ComplexUnitType.Dip, pixels, Resources.DisplayMetrics);
		}


		public void InitNot()
		{

			// GESTIONE NOtifiche
			NotView = FindViewById<RelativeLayout>(Resource.Id.NotificationView);
			ChiudiL = FindViewById<RelativeLayout>(Resource.Id.ChiudiL);
			VediL = FindViewById<RelativeLayout>(Resource.Id.VediL);

			ChiudiTxt = FindViewById<TextView>(Resource.Id.ChiudiTxt);
			VediTxt = FindViewById<TextView>(Resource.Id.VediTxt);
			NotTitTxt = FindViewById<TextView>(Resource.Id.NotificaTit);
			NotTxt = FindViewById<TextView>(Resource.Id.NotificaTxt);

			NotImg = FindViewById<ImageView>(Resource.Id.NotImg);

			ChiudiTxt.Typeface = tfCB;
			VediTxt.Typeface = tfCB;
			NotTitTxt.Typeface = tfCB;
			NotTxt.Typeface = tfCL;

			NotValueAnimator = ValueAnimator.OfInt(0);
			NotValueAnimator.SetDuration(300);
			NotValueAnimator.Update += (sender, e) =>
			{

				RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)NotView.LayoutParameters;
				layoutParams.BottomMargin = -PixelsToDp(60) + PixelsToDp(60 * ((int)e.Animation.AnimatedValue / 100f));
				NotView.LayoutParameters = layoutParams;

			};

			VediL.Click += (sender, e) =>
			{

				if (GlobalStructureInstance.Instance.NotificaObj != null)
				{
					var Not = GlobalStructureInstance.Instance.NotificaObj;

					if (Not.Type == NotificheType.Contratto)
					{
						var intent = new Intent(this, typeof(ContrattiPage));
						intent.PutExtra("contrattiNumber", Not.Id_Object);
						this.StartActivity(intent);
						Finish();
					}

					if (Not.Type == NotificheType.ScadenzaFattura)
					{
						if (!string.IsNullOrEmpty(Not.Id_Object))
						{
							PdfView.Visibility = ViewStates.Visible;
							Pdf.Visibility = ViewStates.Gone;
							Pdf.LoadUrl("http://genio.globalnetitalia.it/ext_documento.aspx?saledocId=" + Not.Id_Object);
							isPdfOpen = true;
						}
					}

					GlobalStructureInstance.Instance.NotificaObj = null;
				}
			};

			ChiudiL.Click += (sender, e) =>
			{

				GlobalStructureInstance.Instance.NotificaObj = null;

				NotValueAnimator.SetIntValues(100, 0);
				NotValueAnimator.Start();

			};

		}

		public void DisplayNot(NotificheObject Not)
		{

			GlobalStructureInstance.Instance.NotificaObj = Not;

			if (Not.Type == NotificheType.Fattura)
			{

				AddFattura(Not);
				GlobalStructureInstance.Instance.NotificaObj = null;
			}

			if (Not.Type == NotificheType.Contratto)
			{
				NotImg.SetImageResource(Resource.Drawable.ContrattiMenu);
				NotTxt.Text = "Scadenza Contratto";

				NotValueAnimator.SetIntValues(0, 100);
				NotValueAnimator.Start();
			}

			if (Not.Type == NotificheType.ScadenzaFattura)
			{
				NotImg.SetImageResource(Resource.Drawable.FattureMenu);
				NotTxt.Text = "Scadenza Fattura";

				NotValueAnimator.SetIntValues(0, 100);
				NotValueAnimator.Start();

			}


		}

		public void AddFattura(NotificheObject Not)
		{

			ListDocumenti = GlobalStructureInstance.Instance.listDocumenti;


			/*DocumentiObj = ListDocumenti[0];

			View InfoDataView = inflater.Inflate(Resource.Layout.FattureTemplate, Cont, false);

			var box = InfoDataView.FindViewById<LinearLayout>(Resource.Id.Box);

			var dataTit = InfoDataView.FindViewById<TextView>(Resource.Id.DataTit);
			var dataTxt = InfoDataView.FindViewById<TextView>(Resource.Id.DataTxt);

			var fatturaTit = InfoDataView.FindViewById<TextView>(Resource.Id.FatturaTit);
			var fatturaTxt = InfoDataView.FindViewById<TextView>(Resource.Id.FatturaTxt);

			var TotaleTit = InfoDataView.FindViewById<TextView>(Resource.Id.TotaleTit);
			var TotaleTxt = InfoDataView.FindViewById<TextView>(Resource.Id.TotaleTxt);

			var newData = InfoDataView.FindViewById<RelativeLayout>(Resource.Id.NewData);
			newData.Visibility = ViewStates.Visible;

			dataTit.Typeface = tfCL;
			dataTxt.Typeface = tfCL;

			fatturaTit.Typeface = tfCL;
			fatturaTxt.Typeface = tfCL;

			TotaleTit.Typeface = tfCL;
			TotaleTxt.Typeface = tfCL;

			//info.Text = "info " + i;
			//data.Text = "data " + i;
			dataTxt.Text = DocumentiObj.DataDocumento.ToString("dd/MM/yy");
			fatturaTxt.Text = DocumentiObj.numero_documento;
			TotaleTxt.Text = "DA TROVARE";

			box.Click += delegate
			{
				PdfView.Visibility = ViewStates.Visible;
				Pdf.Visibility = ViewStates.Gone;
				Pdf.LoadUrl("http://genio.globalnetitalia.it/ext_documento.aspx?saledocId=" + ListDocumenti[indice].SaleDocId);
				isPdfOpen = true;
			};

			Container.AddView(InfoDataView,0);
			*/
			Container.RemoveAllViews();
			var NOFattureTxt = FindViewById<TextView>(Resource.Id.NOFattureTxt);

			if (ListDocumenti.Count != 0)
				NOFattureTxt.Visibility = ViewStates.Gone;

			for (int i = 0; i < ListDocumenti.Count; i++)
			{

				DocumentiObj = ListDocumenti[i];

				View InfoDataView = inflater.Inflate(Resource.Layout.FattureTemplate, Cont, false);

				var box = InfoDataView.FindViewById<LinearLayout>(Resource.Id.Box);

				var dataTit = InfoDataView.FindViewById<TextView>(Resource.Id.DataTit);
				var dataTxt = InfoDataView.FindViewById<TextView>(Resource.Id.DataTxt);

				var fatturaTit = InfoDataView.FindViewById<TextView>(Resource.Id.FatturaTit);
				var fatturaTxt = InfoDataView.FindViewById<TextView>(Resource.Id.FatturaTxt);

				var TotaleTit = InfoDataView.FindViewById<TextView>(Resource.Id.TotaleTit);
				var TotaleTxt = InfoDataView.FindViewById<TextView>(Resource.Id.TotaleTxt);

				var newData = InfoDataView.FindViewById<RelativeLayout>(Resource.Id.NewData);

				if (DocumentiObj.numero_documento != Not.Fattura.numero_documento)
					newData.Visibility = ViewStates.Gone;

				dataTit.Typeface = tfCL;
				dataTxt.Typeface = tfCL;

				fatturaTit.Typeface = tfCL;
				fatturaTxt.Typeface = tfCL;

				TotaleTit.Typeface = tfCL;
				TotaleTxt.Typeface = tfCL;

				//info.Text = "info " + i;
				//data.Text = "data " + i;
				dataTxt.Text = DocumentiObj.DataDocumento.ToString("dd/MM/yy");
				fatturaTxt.Text = DocumentiObj.numero_documento;
				TotaleTxt.Text = "DA TROVARE";

				int indice = i;
				box.Click += delegate
				{
					PdfView.Visibility = ViewStates.Visible;
					Pdf.Visibility = ViewStates.Gone;
					Pdf.LoadUrl("http://genio.globalnetitalia.it/ext_documento.aspx?saledocId=" + ListDocumenti[indice].SaleDocId);
					isPdfOpen = true;
				};

				Container.AddView(InfoDataView);

			}
		
		}
	

	}

	class MyWebViewChromeHome : WebChromeClient
	{
		public MyWebViewChromeHome()
		{
		}
		public override void OnProgressChanged(WebView view, int progress)
		{
			//Console.WriteLine (progress);

			if (progress == 100)
			{
				view.Visibility = ViewStates.Visible;
			}

		}

	}	
}