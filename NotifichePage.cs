﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Animation;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace GlobalNet
{
	[Activity(Label = "NotifichePage", ScreenOrientation = ScreenOrientation.Portrait)]
	public class NotifichePage : Activity
	{

		Typeface tfCL, tfCB;

		RelativeLayout MenuClick, BackClick;
		ImageView MenuImg;

		LayoutInflater inflater;
		ViewGroup Cont;
		LinearLayout Container;

		// MENU VARIABLE 

		RelativeLayout MenuView;
		RelativeLayout Menu; // 200 x 335

		LinearLayout FatturaMenu, ContrattiMenu, DatiMenu, NotificheMenu;
		TextView FatturaTxtMenu, ContrattiTxtMenu, DatiTxtMenu, NotificheTxtMenu;

		ValueAnimator MenuValueAnimator;

		bool MenuOpen = false;

		List<NotificheObject> ListaNotifiche;
		NotificheObject NotificheObj;

		// NOtification VARIABLE 

		RelativeLayout NotView;
		RelativeLayout ChiudiL, VediL;

		TextView ChiudiTxt, VediTxt, NotTitTxt, NotTxt;
		ImageView NotImg;

		ValueAnimator NotValueAnimator;

		public static NotifichePage Instance { private set; get; }


		public bool isVisible = true;

		protected override void OnStop()
		{
			base.OnStop();
			isVisible = false;
		}

		protected override void OnResume()
		{
			base.OnResume();

			isVisible = true;

			GlobalStructureInstance.Instance.SetPage(PageName.Notifiche);
		}
	
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Create your application here
			Window.RequestFeature(WindowFeatures.NoTitle);

			SetContentView(Resource.Layout.NotifichePage);

			GlobalStructureInstance.Instance.SetPage(PageName.Notifiche);

			NotifichePage.Instance = this;

			tfCL = Typeface.CreateFromAsset(Assets, "fonts/OpenSans_CondLight.ttf");
			tfCB = Typeface.CreateFromAsset(Assets, "fonts/OpenSans_CondBold.ttf");

			//NAVIGATION BAR MENU
			MenuClick = FindViewById<RelativeLayout>(Resource.Id.MenuClick);
			MenuImg = FindViewById<ImageView>(Resource.Id.MenuIcon);

			MenuClick.Click += delegate
			{
				OpenMenu();
			};

			//INFO BAR
			BackClick = FindViewById<RelativeLayout>(Resource.Id.BackClick);
			BackClick.Click += delegate
			{
				Finish();
			};

			var NotificheTxt = FindViewById<TextView>(Resource.Id.NotificheTxt);

			NotificheTxt.Typeface = tfCB;

			Container = FindViewById<LinearLayout>(Resource.Id.Container);

			inflater = this.LayoutInflater;
			Cont = (ViewGroup)Container;

			var NONotificheTxt = FindViewById<TextView>(Resource.Id.NONotificheTxt);

			NONotificheTxt.Typeface = tfCL;

			ListaNotifiche = GlobalStructureInstance.Instance.listNotifiche;

			if (ListaNotifiche.Count != 0)
				NONotificheTxt.Visibility = ViewStates.Gone;

			for (int i = 0; i < ListaNotifiche.Count; i++)
			{

				NotificheObj = ListaNotifiche[i];

				View InfoDataView = inflater.Inflate(Resource.Layout.NotificheTemplate, Cont, false);

				var box = InfoDataView.FindViewById<RelativeLayout>(Resource.Id.Box);

				var TypeImg = InfoDataView.FindViewById<ImageView>(Resource.Id.TypeImg);

				var dataTxt = InfoDataView.FindViewById<TextView>(Resource.Id.DataTxt);
				var oraTxt = InfoDataView.FindViewById<TextView>(Resource.Id.OraTxt);

				var notificaTit = InfoDataView.FindViewById<TextView>(Resource.Id.NotificaTit);
				var notificaTxt = InfoDataView.FindViewById<TextView>(Resource.Id.NotificaTxt);

				dataTxt.Typeface = tfCL;
				oraTxt.Typeface = tfCL;

				notificaTit.Typeface = tfCL;
				notificaTxt.Typeface = tfCL;

				dataTxt.Text = NotificheObj.Date.ToString("dd/MM/yyyy");
                oraTxt.Text = "";//NotificheObj.Date.ToString("hh:mm");

				notificaTxt.Text = NotificheObj.Text;

				if (NotificheObj.Type == NotificheType.Fattura)
				{

					TypeImg.SetImageResource(Resource.Drawable.FatturePage);
					notificaTit.Text = "Nuova Fattura";

				}

				if (NotificheObj.Type == NotificheType.Contratto)
				{
					TypeImg.SetImageResource(Resource.Drawable.ContrattiPage);
					notificaTit.Text = "Scadenza Contratto";
				}

				if (NotificheObj.Type == NotificheType.ScadenzaFattura)
				{
					TypeImg.SetImageResource(Resource.Drawable.FatturePage);
					notificaTit.Text = "Scadenza Fattura";
				}


				var newData = InfoDataView.FindViewById<RelativeLayout>(Resource.Id.NewData);

				if (GlobalStructureInstance.Instance.NotificaObj != null)
				{
					var Not = GlobalStructureInstance.Instance.NotificaObj;

					if (Not.Type == NotificheObj.Type && Not.Id_Object == NotificheObj.Id_Object)
					{
						GlobalStructureInstance.Instance.NotificaObj = null;
					}
					else
					{
						newData.Visibility = ViewStates.Gone;
					}
				}
				else
				{
					newData.Visibility = ViewStates.Gone;
				}

				int indice = i;
				var notOBJ = NotificheObj;
				box.Click += delegate
				{
					if (notOBJ.Type == NotificheType.Fattura)
					{
						var intent = new Intent(this, typeof(FatturePage));
						intent.PutExtra("saledocId", notOBJ.Id_Object);
						this.StartActivity(intent);
						Finish();
					}

					if (notOBJ.Type == NotificheType.Contratto)
					{
						var intent = new Intent(this, typeof(ContrattiPage));
						intent.PutExtra("contrattiNumber", notOBJ.Id_Object);
						this.StartActivity(intent);
						Finish();
					}

					if (notOBJ.Type == NotificheType.ScadenzaFattura)
					{
						var intent = new Intent(this, typeof(FatturePage));
						intent.PutExtra("saledocId", notOBJ.Id_Object);
						this.StartActivity(intent);
						Finish();
					}

				};

				Container.AddView(InfoDataView);

			}


			InitMenu();

		}

		public override void OnBackPressed()
		{
			//base.OnBackPressed();

			if (MenuOpen)
			{

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

			}
			else
			{
				Finish();
			}
		}

		public void OpenMenu()
		{

			if (MenuOpen)
			{

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

			}
			else
			{

				MenuView.Visibility = ViewStates.Visible;
				MenuImg.SetImageResource(Resource.Drawable.MenuSpentoIcon);

				MenuValueAnimator.SetIntValues(0, 100);
				MenuValueAnimator.Start();


			}

		}

		public void InitMenu()
		{

			// GESTIONE MENU
			MenuView = FindViewById<RelativeLayout>(Resource.Id.MenuView);
			Menu = FindViewById<RelativeLayout>(Resource.Id.Menu);

			MenuView.Visibility = ViewStates.Gone;
			MenuView.Clickable = true;

			FatturaMenu = FindViewById<LinearLayout>(Resource.Id.FattureMenu);
			ContrattiMenu = FindViewById<LinearLayout>(Resource.Id.ContrattiMenu);
			DatiMenu = FindViewById<LinearLayout>(Resource.Id.DatiMenu);
			NotificheMenu = FindViewById<LinearLayout>(Resource.Id.NotificheMenu);

			FatturaMenu.Click += delegate
			{

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

				var intent = new Intent(this, typeof(FatturePage));
				this.StartActivity(intent);
				Finish();

			};

			ContrattiMenu.Click += delegate
			{

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

				var intent = new Intent(this, typeof(ContrattiPage));
				this.StartActivity(intent);
				Finish();

			};

			DatiMenu.Click += delegate
			{

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

				var intent = new Intent(this, typeof(InfoDataPage));
				this.StartActivity(intent);
				Finish();

			};

			NotificheMenu.Click += delegate
			{

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

			};

            var LogoutMenu = FindViewById<LinearLayout>(Resource.Id.LogoutMenu);
            LogoutMenu.Click += (sender, e) => {

                var client = new RestSharp.RestClient(GlobalStructureInstance.Instance.BaseUrl + "/");

                Console.WriteLine(GlobalStructureInstance.Instance.BaseUrl + "/" + "logout");

                var requestN4U = new RestSharp.RestRequest("logout", RestSharp.Method.GET);
                requestN4U.AddHeader("x-access-token", GlobalStructureInstance.Instance.Token);

                client.ExecuteAsync(requestN4U, (arg1, arg2) => {

                    var response = arg1;

                    Console.WriteLine("StatusCode servizi:" + response.StatusCode +
                                                  "\nContent servizi:" + response.Content);


                    RunOnUiThread(() => {

                        var prefEditor = MainActivity.Instance.prefs.Edit();
                        prefEditor.PutString("TokenN4U", "");
                        prefEditor.Commit();

                        //GlobalStructureInstance.Instance.ClearAll();

                        //MainActivity.Instance.LoadView.Visibility = ViewStates.Gone;

                        HomePage.Instance.Finish();
                        Finish();
                    });
                });

            };
            var LogoutTxtMenu = FindViewById<TextView>(Resource.Id.LogoutTxtMenu);
            LogoutTxtMenu.Typeface = tfCB;

			FatturaTxtMenu = FindViewById<TextView>(Resource.Id.FatturaTxtMenu);
			ContrattiTxtMenu = FindViewById<TextView>(Resource.Id.ContrattiTxtMenu);
			DatiTxtMenu = FindViewById<TextView>(Resource.Id.DatiTxtMenu);
			NotificheTxtMenu = FindViewById<TextView>(Resource.Id.NotificheTxtMenu);

			FatturaTxtMenu.Typeface = tfCB;
			ContrattiTxtMenu.Typeface = tfCB;
			DatiTxtMenu.Typeface = tfCB;
			NotificheTxtMenu.Typeface = tfCB;

			MenuValueAnimator = ValueAnimator.OfInt(0);
			MenuValueAnimator.SetDuration(300);
			MenuValueAnimator.Update += (sender, e) =>
			{

				RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)Menu.LayoutParameters;
                layoutParams.Height = PixelsToDp(4.2f * (int)e.Animation.AnimatedValue);
				layoutParams.Width = PixelsToDp(2 * (int)e.Animation.AnimatedValue);
				Menu.LayoutParameters = layoutParams;

				if (MenuOpen && (int)e.Animation.AnimatedValue == 0)
				{

					MenuOpen = false;

					MenuImg.SetImageResource(Resource.Drawable.MenuIcon);

					MenuView.Visibility = ViewStates.Gone;

				}
				if (!MenuOpen && (int)e.Animation.AnimatedValue == 100)
				{

					MenuOpen = true;

				}

			};

			MenuView.Click += delegate
			{

				MenuValueAnimator.SetIntValues(100, 0);
				MenuValueAnimator.Start();

			};

		}

		private int PixelsToDp(float pixels)
		{
			return (int)Android.Util.TypedValue.ApplyDimension(Android.Util.ComplexUnitType.Dip, pixels, Resources.DisplayMetrics);
		}

		public void InitNot()
		{
			/*
			// GESTIONE NOtifiche
			NotView = FindViewById<RelativeLayout>(Resource.Id.NotificationView);
			ChiudiL = FindViewById<RelativeLayout>(Resource.Id.ChiudiL);
			VediL = FindViewById<RelativeLayout>(Resource.Id.VediL);

			ChiudiTxt = FindViewById<TextView>(Resource.Id.ChiudiTxt);
			VediTxt = FindViewById<TextView>(Resource.Id.VediTxt);
			NotTitTxt = FindViewById<TextView>(Resource.Id.NotificaTit);
			NotTxt = FindViewById<TextView>(Resource.Id.NotificaTxt);

			NotImg = FindViewById<ImageView>(Resource.Id.NotImg);

			ChiudiTxt.Typeface = tfCB;
			VediTxt.Typeface = tfCB;
			NotTitTxt.Typeface = tfCB;
			NotTxt.Typeface = tfCL;

			NotValueAnimator = ValueAnimator.OfInt(0);
			NotValueAnimator.SetDuration(300);
			NotValueAnimator.Update += (sender, e) =>
			{

				RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)NotView.LayoutParameters;
				layoutParams.BottomMargin = -PixelsToDp(60) + PixelsToDp(60 * ((int)e.Animation.AnimatedValue / 100f));
				NotView.LayoutParameters = layoutParams;

			};

			VediL.Click += (sender, e) =>
			{

				if (GlobalStructureInstance.Instance.NotificaObj != null)
				{
					var Not = GlobalStructureInstance.Instance.NotificaObj;

					if (Not.Type == NotificheType.Contratto)
					{
						var intent = new Intent(this, typeof(ContrattiPage));
						intent.PutExtra("contrattiNumber", Not.Id_Object);
						this.StartActivity(intent);
						Finish();
					}

					if (Not.Type == NotificheType.ScadenzaFattura)
					{
						if (!string.IsNullOrEmpty(Not.Id_Object))
						{
							PdfView.Visibility = ViewStates.Visible;
							Pdf.Visibility = ViewStates.Gone;
							Pdf.LoadUrl("http://genio.globalnetitalia.it/ext_documento.aspx?saledocId=" + Not.Id_Object);
							isPdfOpen = true;
						}
					}

					GlobalStructureInstance.Instance.NotificaObj = null;
				}
			};

			ChiudiL.Click += (sender, e) =>
			{

				GlobalStructureInstance.Instance.NotificaObj = null;

				NotValueAnimator.SetIntValues(100, 0);
				NotValueAnimator.Start();

			};*/

		}

		public void DisplayNot(NotificheObject Not)
		{

			GlobalStructureInstance.Instance.NotificaObj = Not;

			AddNotifica(Not);
			GlobalStructureInstance.Instance.NotificaObj = null;

		}

		public void AddNotifica(NotificheObject Not)
		{

			ListaNotifiche = GlobalStructureInstance.Instance.listNotifiche;

			Container.RemoveAllViews();
			var NONotificheTxt = FindViewById<TextView>(Resource.Id.NONotificheTxt);

			if (ListaNotifiche.Count != 0)
				NONotificheTxt.Visibility = ViewStates.Gone;

			for (int i = 0; i < ListaNotifiche.Count; i++)
			{

				NotificheObj = ListaNotifiche[i];

				View InfoDataView = inflater.Inflate(Resource.Layout.NotificheTemplate, Cont, false);

				var box = InfoDataView.FindViewById<RelativeLayout>(Resource.Id.Box);

				var TypeImg = InfoDataView.FindViewById<ImageView>(Resource.Id.TypeImg);

				var dataTxt = InfoDataView.FindViewById<TextView>(Resource.Id.DataTxt);
				var oraTxt = InfoDataView.FindViewById<TextView>(Resource.Id.OraTxt);

				var notificaTit = InfoDataView.FindViewById<TextView>(Resource.Id.NotificaTit);
				var notificaTxt = InfoDataView.FindViewById<TextView>(Resource.Id.NotificaTxt);

				dataTxt.Typeface = tfCL;
				oraTxt.Typeface = tfCL;

				notificaTit.Typeface = tfCL;
				notificaTxt.Typeface = tfCL;

				dataTxt.Text = NotificheObj.Date.ToString("dd/MM/yyyy");
				oraTxt.Text = NotificheObj.Date.ToString("hh:mm");

				notificaTxt.Text = NotificheObj.Text;

				if (NotificheObj.Type == NotificheType.Fattura)
				{

					TypeImg.SetImageResource(Resource.Drawable.FatturePage);
					notificaTit.Text = "Nuova Fattura";

				}

				if (NotificheObj.Type == NotificheType.Contratto)
				{
					TypeImg.SetImageResource(Resource.Drawable.ContrattiPage);
					notificaTit.Text = "Scadenza Contratto";
				}

				if (NotificheObj.Type == NotificheType.ScadenzaFattura)
				{
					TypeImg.SetImageResource(Resource.Drawable.FatturePage);
					notificaTit.Text = "Scadenza Fattura";
				}

				var newData = InfoDataView.FindViewById<RelativeLayout>(Resource.Id.NewData);

				if (GlobalStructureInstance.Instance.NotificaObj != null)
				{

					if (Not.Type == NotificheObj.Type && Not.Id_Object == NotificheObj.Id_Object)
					{
						GlobalStructureInstance.Instance.NotificaObj = null;
					}
					else
					{
						newData.Visibility = ViewStates.Gone;
					}
				}
				else
				{
					newData.Visibility = ViewStates.Gone;
				}
			
				int indice = i;
				var notOBJ = NotificheObj;
				box.Click += delegate
				{
					if (notOBJ.Type == NotificheType.Fattura)
					{
						var intent = new Intent(this, typeof(FatturePage));
						intent.PutExtra("saledocId", notOBJ.Id_Object);
						this.StartActivity(intent);
						Finish();
					}

					if (notOBJ.Type == NotificheType.Contratto)
					{
						var intent = new Intent(this, typeof(ContrattiPage));
						intent.PutExtra("contrattiNumber", notOBJ.Id_Object);
						this.StartActivity(intent);
						Finish();
					}

					if (notOBJ.Type == NotificheType.ScadenzaFattura)
					{
						var intent = new Intent(this, typeof(FatturePage));
						intent.PutExtra("saledocId", notOBJ.Id_Object);
						this.StartActivity(intent);
						Finish();
					}

				};

				Container.AddView(InfoDataView);

			}
		
		}
	
	
	}
}
