﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Graphics;
using Android.Views;
using System;
using Android.Content;
using Android.Content.PM;
using System.Collections.Generic;
using RestSharp;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Globalization;
using System.Linq;
using Gcm.Client;

namespace GlobalNet
{
	[Activity(ScreenOrientation = ScreenOrientation.Portrait)]
	public class MainActivity : Activity, ViewTreeObserver.IOnGlobalLayoutListener
	{

		Typeface tfCL, tfCB;

		EditText EMail, Password;
		Button Login;

		public RelativeLayout LoadView;
		ProgressBar loading;

		RelativeLayout ModalView;
		TextView ModalTitle, ModalDescr, ModalButton;
		EventHandler ModalClick;

		public ISharedPreferences prefs;

		public static MainActivity Instance { get; private set; }

		int i = 0;

		// GESTIONE KEYBOARD
		RelativeLayout content, content2, EditLay;
		bool firstTime = true;
		int bott;


		bool notification;
		string NotificationString;
	
		protected override void OnResume()
		{
			base.OnResume();

            if (GlobalStructureInstance.Instance.Login)
			{
                GlobalStructureInstance.Instance.ClearAll();

                MainActivity.Instance.LoadView.Visibility = ViewStates.Gone;
			}
			content.ViewTreeObserver.AddOnGlobalLayoutListener(this);

		}

		protected override void OnPause()
		{

			base.OnPause();

			content.ViewTreeObserver.RemoveOnGlobalLayoutListener(this);
		}

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			GcmClient.CheckDevice(this);
			GcmClient.CheckManifest(this);		

			Window.RequestFeature(WindowFeatures.NoTitle);

			SetContentView(Resource.Layout.Main);

			tfCL = Typeface.CreateFromAsset(Assets, "fonts/OpenSans_CondLight.ttf");
			tfCB = Typeface.CreateFromAsset(Assets, "fonts/OpenSans_CondBold.ttf");

			GlobalStructureInstance gsi = new GlobalStructureInstance();

			MainActivity.Instance = this;
			prefs = Application.Context.GetSharedPreferences("GlobalNet", FileCreationMode.Private);

			notification = Intent.GetBooleanExtra("notification", false);

			if (notification)
			{
				NotificationString = Intent.GetStringExtra("NotificationString");
			}

			content = FindViewById<RelativeLayout>(Resource.Id.content);
			content.ViewTreeObserver.AddOnGlobalLayoutListener(this);

			content2 = FindViewById<RelativeLayout>(Resource.Id.content2);

			EditLay = FindViewById<RelativeLayout>(Resource.Id.EditLay);

            gsi.BaseUrl = "http://server1.netwintec.com:3001";

			EMail = FindViewById<EditText>(Resource.Id.EmailText);
			Password = FindViewById<EditText>(Resource.Id.PasswordText);

			Login = FindViewById<Button>(Resource.Id.LoginButton);
			bott = Login.Bottom;

            EMail.Text = "3211264_4020";
            Password.Text = "chri1996";

			EMail.Typeface = tfCL;
			Password.Typeface = tfCL;

			EMail.Click += (sender, e) => {
                AlzaView();
			};

			Password.Click += (sender, e) => {
				AlzaView();
			};

			bool first = true;
			EMail.FocusChange += (sender, e) => {
				if (!first)
				{
					if (e.HasFocus)
						AlzaView();
				}
				else
				{
					first = false;
				}
			};

			Password.FocusChange += (sender, e) => {
				Console.WriteLine(e.HasFocus);
				if(e.HasFocus)
					AlzaView();;
			};

			Login.Typeface = tfCB;

			LoadView = FindViewById<RelativeLayout>(Resource.Id.LoadView);
			loading = FindViewById<ProgressBar>(Resource.Id.Loading);

			LoadView.Clickable = true;
			LoadView.Visibility = ViewStates.Gone;
			loading.IndeterminateDrawable.SetColorFilter(Color.Rgb(28, 151, 219), PorterDuff.Mode.Multiply);


			Login.Click += delegate
			{

				LoadView.Visibility = ViewStates.Visible;

				new System.Threading.Thread(new System.Threading.ThreadStart(() =>
				{

					List<string> Res;

					UtilityLoginManager ulm = new UtilityLoginManager();

					Res = ulm.Login(EMail.Text, Password.Text);

					foreach (var s in Res)
						Console.WriteLine(s);

					RunOnUiThread(() =>
					{
						

						if (Res[0] == "ERROR")
						{
							LoadView.Visibility = ViewStates.Gone;

							ModalView.Visibility = ViewStates.Visible;
							ModalTitle.Text = "Errore";
							ModalDescr.Text = "Login Non riuscito riprovare!";
							ModalButton.Text = "OK";
							ModalButton.Click -= ModalClick;
							ModalClick = new EventHandler(delegate
							{
								ModalView.Visibility = ViewStates.Gone;
							});
							ModalButton.Click += ModalClick;
						}



					});

					if (Res[0] == "SUCCESS")
					{
						RunOnUiThread(() =>
						{
                            GlobalStructureInstance.Instance.Token = Res[1];
                            GlobalStructureInstance.Instance.Login = true;

							var prefEditor = prefs.Edit();
							prefEditor.PutString("TokenN4U", Res[1]);
							prefEditor.Commit();

                            GcmClient.Register(this, GcmBroadcastReceiver.SENDER_IDS);


							new System.Threading.Thread(new System.Threading.ThreadStart(() =>
							{
								DownloadData(0);
							})).Start();

						});

					}

				})).Start();

			};


			// GESTIONE MODAL

			ModalView = FindViewById<RelativeLayout>(Resource.Id.ModalView);
			ModalTitle = FindViewById<TextView>(Resource.Id.ModalTitle);
			ModalDescr = FindViewById<TextView>(Resource.Id.ModalDescr);
			ModalButton = FindViewById<TextView>(Resource.Id.ModalConfirm);

			ModalView.Clickable = true;
			ModalView.Visibility = ViewStates.Gone;
			ModalTitle.Typeface = tfCB;
			ModalDescr.Typeface = tfCL;
			ModalButton.Typeface = tfCL;

			ModalButton.Click += ModalClick;

			var token = prefs.GetString("TokenN4U", "");
			Console.WriteLine("TOKEN:" + token);
			if (!string.IsNullOrEmpty(token))
			{

				LoadView.Visibility = ViewStates.Visible;
				GlobalStructureInstance.Instance.Token = token;
				new System.Threading.Thread(new System.Threading.ThreadStart(() =>
				{

					var client = new RestClient(GlobalStructureInstance.Instance.BaseUrl + "/");

                    //Console.WriteLine(GlobalStructureInstance.Instance.BaseUrl + "/" + "anagrafica\n"+
                    //                 "TOKEN:" + GlobalStructureInstance.Instance.Token);

					var requestN4U = new RestRequest("anagrafica", Method.GET);
					requestN4U.AddHeader("x-access-token", GlobalStructureInstance.Instance.Token);
					requestN4U.AddHeader("content-type", "application/x-www-form-urlencoded");

					IRestResponse response = client.Execute(requestN4U);

					//Console.WriteLine("StatusCode anagrafica:" + response.StatusCode +
                    //								  "\nContent anagrafica:" + response.Content);

					if (response.StatusCode == System.Net.HttpStatusCode.OK)
					{

						var StrResponse = response.Content.Substring(1, response.Content.Length - 2);


                        GcmClient.Register(this, GcmBroadcastReceiver.SENDER_IDS);

						GlobalStructureInstance.Instance.listAnagrafica = JsonConvert.DeserializeObject<List<AnagraficaObject>>(StrResponse);
						RunOnUiThread(() =>
						{
							DownloadData(1);
							return;
						});
					}
					else
					{
						RunOnUiThread(() =>
						{
							LoadView.Visibility = ViewStates.Gone;
							GlobalStructureInstance.Instance.Token = "";

							var prefEditor = prefs.Edit();
							prefEditor.PutString("TokenN4U", "");
							prefEditor.Commit();

							notification = false;
							NotificationString = "";
						});
					}
				})).Start();
			}
		}



		public void DownloadData(int Index)
		{

			if (Index == 0)
			{
				var client = new RestClient(GlobalStructureInstance.Instance.BaseUrl + "/");

                //Console.WriteLine(GlobalStructureInstance.Instance.BaseUrl + "/" + "anagrafica");

				var requestN4U = new RestRequest("anagrafica", Method.GET);
				requestN4U.AddHeader("x-access-token", GlobalStructureInstance.Instance.Token);
				requestN4U.AddHeader("content-type", "application/x-www-form-urlencoded");

				IRestResponse response = client.Execute(requestN4U);


                //Console.WriteLine("StatusCode anagrafica:" + response.StatusCode +
                //				  "\nContent anagrafica:" + response.Content);

				if (response.StatusCode == System.Net.HttpStatusCode.OK)
				{
					var StrResponse = response.Content.Substring(1, response.Content.Length - 2);
					GlobalStructureInstance.Instance.listAnagrafica = JsonConvert.DeserializeObject<List<AnagraficaObject>>(StrResponse);
				}
				else
					GlobalStructureInstance.Instance.listAnagrafica = new List<AnagraficaObject>();

                foreach (var a in GlobalStructureInstance.Instance.listAnagrafica)
                {
                    //a.Logger();
                }

				RunOnUiThread(() =>
				{
					DownloadData(Index + 1);
					return;
				});
			}

			if (Index == 1)
			{
				var client = new RestClient(GlobalStructureInstance.Instance.BaseUrl + "/");

                //Console.WriteLine(GlobalStructureInstance.Instance.BaseUrl + "/" + "documenti");

				var requestN4U = new RestRequest("documenti", Method.GET);
				requestN4U.AddHeader("x-access-token", GlobalStructureInstance.Instance.Token);
				requestN4U.AddHeader("content-type", "application/x-www-form-urlencoded");

				IRestResponse response = client.Execute(requestN4U);

                //Console.WriteLine("StatusCode documenti:" + response.StatusCode +
                //								  "\nContent documenti:" + response.Content);

				if (response.StatusCode == System.Net.HttpStatusCode.OK)
				{
                    var StrResponse = response.Content;//.Substring(1, response.Content.Length - 2);
					GlobalStructureInstance.Instance.listDocumenti = JsonConvert.DeserializeObject<List<DocumentiObject>>(StrResponse);
				}
				else
					GlobalStructureInstance.Instance.listDocumenti = new List<DocumentiObject>();

				foreach (var d in GlobalStructureInstance.Instance.listDocumenti)
				{
                    //d.Logger();
					d.DataDocumento = DateTime.ParseExact(d.data_documento, "yyyy-MM-dd'T'hh:mm:ss.fff'Z'", CultureInfo.InvariantCulture);
				}

				RunOnUiThread(() =>
				{
					DownloadData(Index + 1);
					return;
				});
			}

			//DateTime a = new DateTime();
			//a = DateTime.ParseExact("aa", "yyyy-MM-dd'T'hh:mm:ss.fff'Z'", CultureInfo.InvariantCulture);

			if (Index == 2)
			{
				var client = new RestClient(GlobalStructureInstance.Instance.BaseUrl + "/");

                //Console.WriteLine(GlobalStructureInstance.Instance.BaseUrl + "/" + "contratti");

				var requestN4U = new RestRequest("contratti", Method.GET);
				requestN4U.AddHeader("x-access-token", GlobalStructureInstance.Instance.Token);
				requestN4U.AddHeader("content-type", "application/x-www-form-urlencoded");

				IRestResponse response = client.Execute(requestN4U);

                //Console.WriteLine("StatusCode contratti:" + response.StatusCode +
                //												  "\nContent contratti:" + response.Content);

				if (response.StatusCode == System.Net.HttpStatusCode.OK)
				{
					var StrResponse = response.Content.Substring(1, response.Content.Length - 2);
					GlobalStructureInstance.Instance.listContratti = JsonConvert.DeserializeObject<List<ContrattiObject>>(StrResponse);
				}
				else
					GlobalStructureInstance.Instance.listContratti = new List<ContrattiObject>();

				foreach (var c in GlobalStructureInstance.Instance.listContratti)
				{
                    //c.Logger();
                    try
                    {
                        c.DataSottoscrizione = DateTime.ParseExact(c.data_sottoscrizione, "yyyy-MM-dd'T'hh:mm:ss.fff'Z'", CultureInfo.InvariantCulture);
                    }catch(Exception e)
                    {
                        c.DataSottoscrizione = new DateTime();
                    }

                    try
                    {
                        c.DataAttivazione = DateTime.ParseExact(c.data_attivazione, "yyyy-MM-dd'T'hh:mm:ss.fff'Z'", CultureInfo.InvariantCulture);
                    }catch(Exception e)
                    {
                        c.DataAttivazione = new DateTime();
                    }
					//Console.WriteLine("data:" + dat.ToString("D"));

					c.ListSede = new List<SedeObject>();

					SedeObject so = new SedeObject();
					so.nome_sede = c.nome_sede;
					so.ListConsumi = new List<ConsumiObject>();
					so.ListServizi = new List<ServiziObject>();

					c.ListSede.Add(so);

				}

				RunOnUiThread(() =>
				{
					DownloadData(Index + 1);
					return;
				});
			}

			if (Index == 3)
			{
				var client = new RestClient(GlobalStructureInstance.Instance.BaseUrl + "/");

                //Console.WriteLine(GlobalStructureInstance.Instance.BaseUrl + "/" + "consumi");

				var requestN4U = new RestRequest("consumi", Method.GET);
				requestN4U.AddHeader("x-access-token", GlobalStructureInstance.Instance.Token);
				requestN4U.AddHeader("content-type", "application/x-www-form-urlencoded");

				IRestResponse response = client.Execute(requestN4U);

                //Console.WriteLine("StatusCode consumi:" + response.StatusCode +
                //				  "\nContent consumi:" + response.Content);

				if (response.StatusCode == System.Net.HttpStatusCode.OK)
				{
					var StrResponse = response.Content.Substring(1, response.Content.Length - 2);
					GlobalStructureInstance.Instance.listConsumi = JsonConvert.DeserializeObject<List<ConsumiObject>>(StrResponse);
				}
				else
					GlobalStructureInstance.Instance.listConsumi = new List<ConsumiObject>();

				foreach (var c in GlobalStructureInstance.Instance.listConsumi)
				{
                    //c.Logger();
                    try
                    {
                        c.DataDisattivazione = DateTime.ParseExact(c.dataDisattivazione, "yyyy-MM-dd'T'hh:mm:ss.fff'Z'", CultureInfo.InvariantCulture);
                    }
                    catch (Exception e)
                    {
                        c.DataDisattivazione = new DateTime();
                    }

                    try{
					    c.DataAttivazione = DateTime.ParseExact(c.dataAttivazione, "yyyy-MM-dd'T'hh:mm:ss.fff'Z'", CultureInfo.InvariantCulture);
                    }
                    catch (Exception e)
                    {
                        c.DataAttivazione = new DateTime();
                    }

					SedeObject sede = null;
					bool NotFound = false;
					try
					{
						var contr = GlobalStructureInstance.Instance.listContratti
														   .Where(obj => obj.nome_sede == c.nome_sede &&
																  obj.numero_contratto == c.numero_contratto).First();

						sede = contr.ListSede.Where(obj => obj.nome_sede == c.nome_sede).First();

					}
					catch (Exception e)
					{
						NotFound = true;
					}


					if (!NotFound && sede != null)
					{
						sede.ListConsumi.Add(c);
					}

				}
				RunOnUiThread(() =>
				{
					DownloadData(Index + 1);
					return;
				});
			}

			if (Index == 4)
			{
				var client = new RestClient(GlobalStructureInstance.Instance.BaseUrl + "/");

                //Console.WriteLine(GlobalStructureInstance.Instance.BaseUrl + "/" + "servizi");

				var requestN4U = new RestRequest("servizi", Method.GET);
				requestN4U.AddHeader("x-access-token", GlobalStructureInstance.Instance.Token);
				requestN4U.AddHeader("content-type", "application/x-www-form-urlencoded");

				IRestResponse response = client.Execute(requestN4U);

                //Console.WriteLine("StatusCode servizi:" + response.StatusCode +
                //								  "\nContent servizi:" + response.Content);

				if (response.StatusCode == System.Net.HttpStatusCode.OK)
				{
					var StrResponse = response.Content.Substring(1, response.Content.Length - 2);
					GlobalStructureInstance.Instance.listServizi = JsonConvert.DeserializeObject<List<ServiziObject>>(StrResponse);
				}
				else
					GlobalStructureInstance.Instance.listServizi = new List<ServiziObject>();

				foreach (var s in GlobalStructureInstance.Instance.listServizi)
				{
                    //s.Logger();
                    try
                    {
                        s.DataDisattivazione = DateTime.ParseExact(s.disattivazione, "yyyy-MM-dd'T'hh:mm:ss.fff'Z'", CultureInfo.InvariantCulture);
                    }
                    catch (Exception e)
                    {
                        s.DataDisattivazione = new DateTime();
                    }
                    try
                    {
                        s.DataDecorrenza = DateTime.ParseExact(s.decorrenza, "yyyy-MM-dd'T'hh:mm:ss.fff'Z'", CultureInfo.InvariantCulture);
                    }
                    catch (Exception e)
                    {
                        s.DataDecorrenza = new DateTime();
                    }

					SedeObject sede = null;
					bool NotFound = false;
					try
					{
						var contr = GlobalStructureInstance.Instance.listContratti
														   .Where(obj => obj.nome_sede == s.nome_sede &&
																  obj.numero_contratto == s.numero_contratto).First();

						contr.totale += int.Parse(s.importo);

						sede = contr.ListSede.Where(obj => obj.nome_sede == s.nome_sede).First();

					}
					catch (Exception e)
					{
						NotFound = true;
					}


					if (!NotFound && sede != null)
					{
						sede.ListServizi.Add(s);
					}
				}

				RunOnUiThread(() =>
				{
					DownloadData(Index + 1);
					return;
				});
			}

			if (Index == 5)
			{


                var client = new RestClient(GlobalStructureInstance.Instance.BaseUrl + "/");

                //Console.WriteLine(GlobalStructureInstance.Instance.BaseUrl + "/" + "notifiche");

                var requestN4U = new RestRequest("notifiche", Method.GET);
                requestN4U.AddHeader("x-access-token", GlobalStructureInstance.Instance.Token);
                requestN4U.AddHeader("content-type", "application/x-www-form-urlencoded");

                IRestResponse response = client.Execute(requestN4U);

                //Console.WriteLine("StatusCode notifiche:" + response.StatusCode +
                //                  "\nContent notifiche:" + response.Content);

                List<NotificheObject_v2> list = new List<NotificheObject_v2>();
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var StrResponse = response.Content.Substring(1, response.Content.Length - 2);
                    try
                    {
                        list = JsonConvert.DeserializeObject<List<NotificheObject_v2>>(StrResponse);
                    }catch(Exception exc)
                    {
                        GlobalStructureInstance.Instance.listNotifiche = new List<NotificheObject>();
                    }
                }
                else
                    GlobalStructureInstance.Instance.listNotifiche = new List<NotificheObject>();

                foreach (var n in list)
                {
                    NotificheObject notifiche = new NotificheObject();

                    notifiche.Type = n.type;
                    notifiche.Text = n.text;
                    notifiche.Id_Object = n.id_object;
                    notifiche.Date = DateTime.ParseExact(n.date_new, "yyyy-MM-dd",null);

                    GlobalStructureInstance.Instance.listNotifiche.Add(notifiche);
                }

                RunOnUiThread(() =>
                {
                    DownloadData(Index + 1);
                    return;
                });
            
                /*

Per entrare sull'app:
3211264_4011 

Password:
6PGdc0tU 

*/
				//var l = new List<NotificheObject>();

			

				//GlobalStructureInstance.Instance.listNotifiche = l;
                //RunOnUiThread(() =>
                //{
                //	DownloadData(Index + 1);
                //	return;
                //});
			}
		
			if (Index == 6)
			{
				RunOnUiThread(() =>
				{
					LoadView.Visibility = ViewStates.Gone;

					var intent = new Intent(this, typeof(HomePage));

					if (notification)
					{
						intent.PutExtra("notification", true);
						intent.PutExtra("NotificationString", NotificationString);
					}

					this.StartActivity(intent);
				});
			}
		}


		public void AlzaView()
		{

			Rect r = new Rect();
			content.GetWindowVisibleDisplayFrame(r);
			int screenHeight = content.RootView.Height;

			if (bott == 0)
			{
				var a = EditLay.Top;
				bott = Login.Bottom;
				bott += a;
			}

			int diff = (int)r.Bottom - bott;

			Console.WriteLine("METOD:"+r.Bottom + "|" + bott + "|" + diff);
			if (diff < 0)
			{
				RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)content2.LayoutParameters;
				layoutParams.TopMargin = diff - PixelsToDp(50);
				content2.LayoutParameters = layoutParams;
			}
			else
			{
				RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)content2.LayoutParameters;
				layoutParams.TopMargin = 0;
				content2.LayoutParameters = layoutParams;
			}

		}

		int KeyHeightOld;

		public void OnGlobalLayout()
		{
			Rect r = new Rect();
			content.GetWindowVisibleDisplayFrame(r);
			int screenHeight = content.RootView.Height;

			if (bott == 0)
			{
				var a = EditLay.Top;
				bott = Login.Bottom;
				bott += a;
			}

			Rect r2 = new Rect();
			Login.GetWindowVisibleDisplayFrame(r2);

			int keypadHeight = screenHeight - r.Bottom;
			int diff = (int)r.Bottom - bott;

			Console.WriteLine("OVERRIDE:"+r.Bottom + "|" + bott + "|" + diff);

			if (KeyHeightOld != keypadHeight)
			{

				KeyHeightOld = keypadHeight;

				if (keypadHeight > screenHeight * 0.15)
				{

					Console.WriteLine("OVERRIDE2:"+ diff);

					if (diff < 0)
					{
						Console.WriteLine("OVERRIDE3:"+ (diff- PixelsToDp(50)));
						RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)content2.LayoutParameters;
						layoutParams.TopMargin = diff - PixelsToDp(50);
						content2.LayoutParameters = layoutParams;
					}
					else
					{
						Console.WriteLine("OVERRIDE4");
						RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)content2.LayoutParameters;
						layoutParams.TopMargin = 0;
						content2.LayoutParameters = layoutParams;
					}

				}
				else
				{
					Console.WriteLine("OVERRIDE5");
					RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)content2.LayoutParameters;
					layoutParams.TopMargin = 0;
					content2.LayoutParameters = layoutParams;
				}
			}
			else
			{

				KeyHeightOld = keypadHeight;

			}


		}

		private int PixelsToDp(int pixels)
		{
			return (int)Android.Util.TypedValue.ApplyDimension(Android.Util.ComplexUnitType.Dip, pixels, Resources.DisplayMetrics);
		}

	}
}

