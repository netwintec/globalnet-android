﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace GlobalNet
{
	[Activity(Label = "NotificationActivity")]
	public class NotificationActivity : Activity
	{
		bool notification;
		string NotificationString;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			notification = Intent.GetBooleanExtra("notification", false);
			if (notification)
			{
				NotificationString = Intent.GetStringExtra("NotificationString");
			}

			if (notification)
			{

				NotificheObject Not;

				try
				{
					Not = JsonConvert.DeserializeObject<NotificheObject>(NotificationString);

					if (GlobalStructureInstance.Instance.isDati)
						InfoDataPage.Instance.Finish();
					if (GlobalStructureInstance.Instance.isFattura)
						FatturePage.Instance.Finish();
					if (GlobalStructureInstance.Instance.isContratti)
						ContrattiPage.Instance.Finish();
					if (GlobalStructureInstance.Instance.isNotifiche)
						NotifichePage.Instance.Finish();
				
					if (Not.Type == NotificheType.Fattura)
					{
						var intent = new Intent(this, typeof(FatturePage));
						intent.PutExtra("saledocId", Not.Id_Object);
						this.StartActivity(intent);
					}

					if (Not.Type == NotificheType.Contratto)
					{
						var intent = new Intent(this, typeof(ContrattiPage));
						intent.PutExtra("contrattiNumber", Not.Id_Object);
						this.StartActivity(intent);
					}

					if (Not.Type == NotificheType.ScadenzaFattura)
					{
						var intent = new Intent(this, typeof(FatturePage));
						intent.PutExtra("saledocId", Not.Id_Object);
						this.StartActivity(intent);
					}

					notification = false;
					NotificationString = "";
					Finish();

				}
				catch (Exception e)
				{
					notification = false;
					NotificationString = "";
					Finish();
				}

			}

			// Create your application her
		}
	}
}
