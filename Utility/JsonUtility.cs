﻿using System;
using System.Collections.Generic;
using System.Json;
using System.IO;


namespace GlobalNet
{		
	public class JsonUtility
	{
		public JsonUtility(){
			
		}

		/*public void SpacchettamentoJsonMarker(string localita,Dictionary<string,MarkerInfo> MarkerDictionary,Dictionary<int,MarkerInfo> MarkerDictionaryComplete,string response,string country){

			//StreamReader strm = new StreamReader (Assets.Open ("Marker.json"));
			//String response = strm.ReadToEnd ();

			JsonValue json = JsonValue.Parse (response);
			//Console.WriteLine (response.Content);
			JsonValue data = json [localita];

			int z = 0;  
			int x = 0;

			foreach (JsonValue dataItem in data) {

				MarkerInfo miC = new MarkerInfo ();

				miC.nome = dataItem ["nome"];
				miC.isHeader = true;

				MarkerDictionaryComplete.Add (x, miC);

				x++;

				JsonValue data2 = dataItem ["elementi"];

				foreach (JsonValue dataItem2 in data2) {

					MarkerInfo mi = new MarkerInfo ();
					mi.nome = miC.nome;
					mi.isHeader = false;

					mi.nomeE = dataItem2 ["nomeE"];
					mi.lat = dataItem2 ["lat"];
					mi.lon = dataItem2 ["lon"];
					mi.via = dataItem2 ["via"];
					mi.titolo = dataItem2 ["titolo"];
					if (country.CompareTo ("IT") == 0) {
						mi.descrizione = dataItem2 ["descrizione_it"];
						mi.descrizioneC = dataItem2 ["descrizioneC_it"];
					} else {
						mi.descrizione = dataItem2 ["descrizione_en"];
						mi.descrizioneC = dataItem2 ["descrizioneC_en"];
					}

					JsonValue data3 = dataItem2 ["urlimg"];

					foreach (JsonValue dataItem3 in data3) {
						mi.imgurl.Add (dataItem3);
						Console.WriteLine (dataItem3.ToString());
					}


					Console.WriteLine (mi.nome + " " + mi.nomeE + " " + mi.lat + " " + mi.lon + " " + mi.titolo + " " + mi.descrizione + " " + mi.descrizioneC + " ");
					MarkerDictionary.Add ("m"+z.ToString(), mi);
					MarkerDictionaryComplete.Add (x, mi);
					x++;
					z++;

				}

			}
		}


		public void SpacchettamentoJsonBeacon(string localita,Dictionary<int,Stanza> StanzeDictionary,Dictionary<int,bool> flag,string content,string country){


			JsonValue json = JsonValue.Parse (content);
			//Console.WriteLine (response.Content);
			JsonValue data = json ["messages"];

			 

			foreach (JsonValue dataItem in data) {
				Stanza st = new Stanza ();

				int z = 0;
				string museo = dataItem ["museo"];

				if (museo == localita) {

					st.stanza = dataItem ["stanza"];
					st.piano = dataItem ["piano"];
					st.img_file = dataItem ["image_url"];
					st.museo = museo;

					JsonValue data3 = dataItem ["coordinate"];

					foreach (JsonValue dataItem3 in data3) {

						float lon = dataItem3 ["lon"];
						float lat = dataItem3 ["lat"];

						st.Poligono.Add (new Location (lat, lon));
					}

					JsonValue data2 = dataItem ["elementi"];

					foreach (JsonValue dataItem2 in data2) {
						
						BeaconInfo bi = new BeaconInfo ();
						bi.stanza = st.stanza;
						bi.tipo = dataItem2 ["tipo"];
						if (bi.tipo == "immagine") {
							bi.url_thumbnail = dataItem2 ["thumbnail"];
							bi.urlImage = dataItem2 ["image"];
							if (country.CompareTo ("IT") == 0) {
								bi.titolo = dataItem2 ["titolo_it"];
								bi.descrizione = dataItem2 ["descrizione_it"];
								bi.urlaudio = dataItem2 ["it"];
							} else {
								bi.titolo = dataItem2 ["titolo_en"];
								bi.descrizione = dataItem2 ["descrizione_en"];
								bi.urlaudio = dataItem2 ["en"];
							}
						}

						if (bi.tipo == "realta") {

							bi.urlImage = dataItem2 ["image"];
							if (country.CompareTo ("IT") == 0) {
								bi.titolo = dataItem2 ["titolo_it"];
								bi.descrizione = dataItem2 ["descrizione_it"];
								bi.urlaudio = dataItem2 ["it"];
							} else {
								bi.titolo = dataItem2 ["titolo_en"];
								bi.descrizione = dataItem2 ["descrizione_en"];
								bi.urlaudio = dataItem2 ["en"];
							}
							bi.urlImage = dataItem2 ["image"];
							bi.url_thumbnail = dataItem2 ["thumbnail"];
							bi.folder3D = dataItem2 ["folder"];

						}

						if (bi.tipo == "immagini") {
							//bi.titolo = dataItem2 ["titolo"];
							bi.url_thumbnail = dataItem2 ["thumbnail"];
							JsonValue image = dataItem2 ["image"];
							List<String> imgApp = new List<string> ();
							int i = 0;
							foreach (JsonValue dataItem3 in image) {
								Console.WriteLine (i + ":" + image [i]);
								imgApp.Add (image [i]);
								i++;
							}
							bi.ListUrlImage = imgApp;
							if (country.CompareTo ("IT") == 0) {
								bi.titolo = dataItem2 ["titolo_it"];
								bi.descrizione = dataItem2 ["descrizione_it"];
								bi.urlaudio = dataItem2 ["it"];
							} else {
								bi.titolo = dataItem2 ["titolo_en"];
								bi.descrizione = dataItem2 ["descrizione_en"];
								bi.urlaudio = dataItem2 ["en"];
							}
						}

						if (bi.tipo == "video") {
							//bi.titolo = dataItem2 ["titolo"];
							bi.url_thumbnail = dataItem2 ["thumbnail"];
							bi.videoUrl = dataItem2 ["url"];
							if (country.CompareTo ("IT") == 0) {
								bi.titolo = dataItem2 ["titolo_it"];
								bi.descrizione = dataItem2 ["descrizione_it"];
							} else {
								bi.titolo = dataItem2 ["titolo_en"];
								bi.descrizione = dataItem2 ["descrizione_en"];
							}
						}
							
						Console.WriteLine (bi.stanza + "|" + bi.tipo + "|" + bi.titolo + "|" + bi.url_thumbnail + "|" + bi.urlImage + "|" + bi.descrizione + "|" + bi.urlaudio + "|" + bi.folder3D + "|");
						st.BeaconDictionary.Add (z, bi);
						z++;

					}

					StanzeDictionary.Add (st.stanza,st);
					flag.Add(st.stanza,false);
				}
			}
		}



		public void SpacchettamentoJsonGioca(List<List<GiocaElement>> listGioca,List<GiocaResult> listResult,string response,string country){

			//String response = strm.ReadToEnd ();

			JsonValue json = JsonValue.Parse (response);

			JsonValue data = json ["elementi"];

			foreach (JsonValue dataItem in data) {

				string museo = dataItem["museo"];
				int active = dataItem["isActive"];

				string result ="";
				string resultImage = "";
				string resultDescr = "";

				try{
					JsonValue data3 = dataItem ["risultato"];

					resultImage = data3["result_Image"];
					if (country.CompareTo ("IT") == 0) {
						result = data3["result_it"];
						resultDescr = data3["result_descrizione_it"];
					}else{
						result = data3["result_en"];
						resultDescr = data3["result_descrizione_en"];
					}

				}catch(Exception e){}

				listResult.Add(new GiocaResult(museo,active,result,resultDescr,resultImage));

				List<GiocaElement> App = new List<GiocaElement> ();

				JsonValue data2 = dataItem ["gioca"];

				foreach (JsonValue dataItem2 in data2) {

					string domanda = "";
					string risposta1 = "";
					string risposta2 = "";
					string risposta3 = "";

					if (country.CompareTo ("IT") == 0) {

						domanda = dataItem2 ["domanda_it"];
						JsonValue ArrayRisposta = dataItem2 ["risposte_it"];
						risposta1 = ArrayRisposta [0];
						risposta2 = ArrayRisposta [1];
						risposta3 = ArrayRisposta [2];

					} else {
						
						domanda = dataItem2 ["domanda_en"];
						JsonValue ArrayRisposta = dataItem2 ["risposte_en"];
						risposta1 = ArrayRisposta [0];
						risposta2 = ArrayRisposta [1];
						risposta3 = ArrayRisposta [2];

					}

					App.Add (new GiocaElement(domanda,risposta1,risposta2,risposta3));
						
				}

				listGioca.Add (App);

			}
		}
*/

	}
}


