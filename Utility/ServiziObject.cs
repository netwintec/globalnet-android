﻿using System;
namespace GlobalNet
{
	public class ServiziObject
	{
		public string nome_sede { get; set; }
		public string numero_contratto { get; set; }
		public string descrizione { get; set; }
		public string quantità { get; set; }
		public string codice { get; set; }
		public string importo { get; set; }
		public string decorrenza { get; set; }
		public string disattivazione { get; set; }
		public string ricorrenza { get; set; }
		public string stato_contratti_servizi { get; set; }

		public DateTime DataDecorrenza { get; set; }
		public DateTime DataDisattivazione { get; set; }
	
		public void Logger()
		{

			Console.WriteLine("** SERVIZIO **\n"+
			                 "nome_sede:"+nome_sede+"\n"+
			                  "numero_contratto:"+numero_contratto+"\n"+
			                  "descrizione:"+descrizione+"\n"+
			                  "quantità:"+quantità+"\n"+
			                  "codice:"+codice+"\n"+
			                  "importo:"+importo+"\n"+
			                  "decorrenza:"+decorrenza+"\n"+
			                  "disattivazione:"+disattivazione+"\n"+
			                  "ricorrenza:"+ricorrenza +"\n"+
			                  "stato_contratti_servizi:"+stato_contratti_servizi);

		}
	}
}
