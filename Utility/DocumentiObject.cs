﻿using System;
namespace GlobalNet
{
	public class DocumentiObject
	{

		public string tipo_documento { get; set; }
		public string numero_documento { get; set; }
		public string data_documento { get; set; }
		public string codice_cliente { get; set; }
        public int SaleDocId { get; set; }
        public float totale_ivato { get; set; }


		public DateTime DataDocumento { get; set; }

		public void Logger()
		{

			Console.WriteLine("** DOCUMENTO **\n"+
			                  "tipo_documento:"+tipo_documento+"\n"+
			                  "numero_documento:"+numero_documento+"\n"+
			                  "data_documento:"+data_documento+"\n"+
			                  "codice_cliente:"+codice_cliente+"\n"+
                              "SaleDocId:"+SaleDocId+ "\n" +
                              "totale_ivato:" + totale_ivato);
		}
	}
}
