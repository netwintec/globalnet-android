﻿using System;
using System.Collections.Generic;

namespace GlobalNet
{
	public class ContrattiObject
	{

		public string clifor { get; set; }
		public string numero_contratto { get; set; }
		public string descrizione_contratto { get; set; }
		public string data_sottoscrizione  { get; set; }
		public string data_attivazione { get; set; }
		public string stato { get; set; }
		public string durata { get; set; }
		public string contrattosito_id { get; set; }
		public string nome_sede  { get; set; }
		public int totale  { get; set; }

		public DateTime DataSottoscrizione { get; set; }
		public DateTime DataAttivazione { get; set; }

		public List<SedeObject> ListSede { get; set; }
	
		public void Logger()
		{

			Console.WriteLine("** CONTRATTO **\n"+
			                 "clifor:"+clifor+"\n"+
			                  "numero_contratto:"+numero_contratto+"\n"+
			                  "descrizione_contratto:"+descrizione_contratto+"\n"+
			                  "data_sottoscrizione:"+data_sottoscrizione+"\n"+
			                  "data_attivazione:"+data_attivazione+"\n"+
			                  "stato:"+stato+"\n"+
			                  "durata:"+durata+"\n"+
			                  "contrattosito_id:"+contrattosito_id+"\n"+
			                  "nome_sede:"+nome_sede);
		}
	}


	public class SedeObject { 
	
		public string nome_sede { get; set; }
		public List<ConsumiObject> ListConsumi { get; set; }
		public List<ServiziObject> ListServizi{ get; set; }

	}
}
