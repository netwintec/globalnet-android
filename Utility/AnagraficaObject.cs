﻿using System;
namespace GlobalNet
{
	public class AnagraficaObject
	{
		public string RagioneSociale { get; set; }
		public string CodiceCliente { get; set; }
		public string Indirizzo { get; set; }
		public string telefono { get; set; }
		public string fax { get; set; }
		public string email { get; set; }
		public string CodiceFiscale { get; set; }
		public string PartitaIva { get; set; }
		public string condizioni_pagamento { get; set; }
		public string iban { get; set; }
		public string Commerciale { get; set; }
		public string Cellulare { get; set; }
		public string EmailCommerciale { get; set; }

		public void Logger() {

			Console.WriteLine("** ANAGRAFICA **\n"+
				"RagioneSociale:"+RagioneSociale+"\n"+
				"CodiceCliente:"+CodiceCliente+"\n"+
				"Indirizzo:"+Indirizzo+"\n"+
				"telefono:"+telefono+"\n"+
				"fax:"+fax+"\n"+
				"email:"+email+"\n"+
				"CodiceFiscale:"+CodiceFiscale+"\n"+
				"PartitaIva:"+PartitaIva+"\n"+
				"condizioni_pagamento:"+condizioni_pagamento+"\n"+
				"iban:"+iban+"\n"+
				"Commerciale:"+Commerciale+"\n"+
				"Cellulare:"+Cellulare+"\n"+
				"EmailCommerciale:"+EmailCommerciale
			);
		}
	}
}