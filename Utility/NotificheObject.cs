﻿using System;
namespace GlobalNet
{
	public class NotificheObject
	{
		public string Type { get; set; }
		public string Text { get; set; }
		public string date { get; set; }
        public string Id_Object { get; set; }

		public DateTime Date { get; set; }


		public DocumentiObject Fattura { get; set; }
	}

    public class NotificheObject_v2
    {
        public string date_new { get; set; }
        public string type { get; set; }
        public string id_object { get; set; }
        public string text { get; set; }

        public DocumentiObject Fattura { get; set; }
    }

	public static class NotificheType{

		public static string Contratto = "contratto";

		public static string Fattura = "fattura";

		public static string ScadenzaFattura = "scadenza_fattura";

	}
}
