﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using System.Net.Mail;
using System.Json;
using RestSharp;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace GlobalNet
{	
	public class UtilityLoginManager  
	{
		bool flagNome=false,flagCognome=false,flagUser=false,flagPW=false;

		public UtilityLoginManager() { }

		public List<string> Login(string username, string pass)
		{
			Console.WriteLine(username + "|" + pass);

			flagPW = false;
			flagUser = false;

			if (pass == null || pass == "")
			{
				flagPW = true;
			}
			if (username == null || username == "")
			{
				flagUser = true;
			}
			else
			{
				/*try
				{
					MailAddress m = new MailAddress(Email);
				}
				catch (FormatException)
				{
					flagEMail = true;
				}*/
			}

			if (flagPW == true || flagUser == true)
			{

				List<string> content = new List<string>();
				content.Add("ERRORDATA");
				return content;

			}


			var client = new RestClient(GlobalStructureInstance.Instance.BaseUrl + "/");
			//client.Authenticator = new HttpBasicAuthenticator(username, password);

			Console.WriteLine(GlobalStructureInstance.Instance.BaseUrl + "/" + "login");

			var requestN4U = new RestRequest("login", Method.POST);
			requestN4U.AddHeader("content-type", "application/json");

			JObject oJsonObject = new JObject();

			oJsonObject.Add("username", username);
			oJsonObject.Add("password", pass);

			requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);


			IRestResponse response = client.Execute(requestN4U);

			Console.WriteLine("Result Login:" + response.Content+"|"+response.StatusCode);

			if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
			{

				List<string> content = new List<string>();
				content.Add("ERROR");
				return content;

			}

			if (response.StatusCode == System.Net.HttpStatusCode.OK)
			{

				List<string> content = new List<string>();
				content.Add("SUCCESS");
				content.Add(response.Content);


				return content;

			}

			List<string> contentErr = new List<string>();
			contentErr.Add("ERROR");
			return contentErr;

		}
	}
}