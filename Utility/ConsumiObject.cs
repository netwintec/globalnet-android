﻿using System;
namespace GlobalNet
{
	public class ConsumiObject
	{

		public string nome_sede { get; set; }
		public string numero_contratto { get; set; }
		public string cli { get; set; }
		public string piano_tariffario { get; set; }
		public string tipologia_servizio { get; set; }
		public string dataAttivazione { get; set; }
		public string dataDisattivazione { get; set; }
		public string stato_contratti_servizi { get; set; }

		public DateTime DataAttivazione { get; set; }
		public DateTime DataDisattivazione { get; set; }

		public void Logger()
		{

			Console.WriteLine("** CONSUMO **\n"+
			                 "nome_sede:"+nome_sede+"\n"+
			                  "numero_contratto:"+numero_contratto+"\n"+
			                  "cli:"+cli+"\n"+
			                  "piano_tariffario:"+piano_tariffario+"\n"+
			                  "tipologia_servizio:"+tipologia_servizio+"\n"+
			                  "dataAttivazione:"+dataAttivazione+"\n"+
			                  "dataDisattivazione:"+dataDisattivazione+"\n"+
			                  "stato_contratti_servizi:"+stato_contratti_servizi);
		}
	}
}
