﻿using System;
using System.Collections.Generic;

namespace GlobalNet
{
	// CLASSE GLOBALE DOVE VENGONO SALVATI LOCALI LE CONVERSAZIONI
	public class GlobalStructureInstance
	{
		
		//BaseUrl chiamate api
		public string BaseUrl;
		//TOken Autenticazione
        public string Token;
        //TokenAPN
        public string APNToken;
		// LISTA ANAGRAFICA
		public List<AnagraficaObject> listAnagrafica;
		// LISTA DOCUMENTI
		public List<DocumentiObject> listDocumenti;
		// LISTA CONTRATTI
		public List<ContrattiObject> listContratti;
		// LISTA CONSUMI
		public List<ConsumiObject> listConsumi;
		// LISTA SERVIZI
		public List<ServiziObject> listServizi;
		// LISTA NOTIFICHE
		public List<NotificheObject> listNotifiche;

		// DATI PER VEDERE LA NOTIFICA NUOVA
		public NotificheObject NotificaObj;

        public bool Login, SendToken;

		// BOOLEANI PER SAPERE DOVE SEI
		public bool isHome, isDati, isFattura;
		public bool isContratti, isNotifiche;


		public static GlobalStructureInstance Instance { private set; get;}

		// RICHIAMABILE CON GlobalStructureInstance.Instance
		public GlobalStructureInstance()
		{
			
			Instance = this;

			Token = "";
			BaseUrl = "";

            Login = false;
            SendToken = false;
			isHome = false;
			isDati = false;
			isFattura = false;
			isContratti = false;
			isNotifiche = false;

			NotificaObj = null;


            listAnagrafica = new List<AnagraficaObject>();
            listDocumenti = new List<DocumentiObject>();
            listContratti = new List<ContrattiObject>();
            listConsumi = new List<ConsumiObject>();
            listServizi = new List<ServiziObject>();
            listNotifiche = new List<NotificheObject>();
		}

		public void ClearAll() {

			Token = "";

            Login = false;
            SendToken = false;

			isHome = false;
			isDati = false;
			isFattura = false;
			isContratti = false;
			isNotifiche = false;

			NotificaObj = null;

		}

		public void SetPage(string name)
		{

			isHome = false;
			isDati = false;
			isFattura = false;
			isContratti = false;
			isNotifiche = false;

			if (name == PageName.Home)
				isHome = true;

			if (name == PageName.Dati)
				isDati = true;

			if (name == PageName.Fatture)
				isFattura = true;

			if (name == PageName.Contratti)
				isContratti = true;

			if (name == PageName.Notifiche)
				isNotifiche = true;
		
		}
	}

	public static class PageName
	{

		public static string Home = "home";

		public static string Dati = "dati";

		public static string Fatture = "fatture";

		public static string Contratti = "contratti";

		public static string Notifiche = "notifiche";
	}
}
