
using System.Text;
using System;
using System.Threading;
using Android.App;
using Android.Content;
using Android.Util;
using Gcm.Client;
using Java.Util;
using Android.Media;
using Android.Content.Res;
using System.Json;
using RestSharp;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;

[assembly: UsesPermission (Android.Manifest.Permission.ReceiveBootCompleted)]

namespace GlobalNet
{
	//You must subclass this!
	[BroadcastReceiver(Permission=Constants.PERMISSION_GCM_INTENTS)]
	[IntentFilter(new string[] { Constants.INTENT_FROM_GCM_MESSAGE }, Categories = new string[] { "@PACKAGE_NAME@" })]
	[IntentFilter(new string[] { Constants.INTENT_FROM_GCM_REGISTRATION_CALLBACK }, Categories = new string[] { "@PACKAGE_NAME@" })]
	[IntentFilter(new string[] { Constants.INTENT_FROM_GCM_LIBRARY_RETRY }, Categories = new string[] { "@PACKAGE_NAME@" })]

	public class GcmBroadcastReceiver : GcmBroadcastReceiverBase<PushHandlerService>
	{
		//IMPORTANT: Change this to your own Sender ID!
		//The SENDER_ID is your Google API Console App Project ID.
		//  Be sure to get the right Project ID from your Google APIs Console.  It's not the named project ID that appears in the Overview,
		//  but instead the numeric project id in the url: eg: https://code.google.com/apis/console/?pli=1#project:738129444068:overview
		//  where 785671162406 is the project id, which is the SENDER_ID to use!
		public static string[] SENDER_IDS = new string[] {"738129444068"};

		public const string TAG = "PushSharp-GCM";
	}

	[Service] //Must use the service tag
	public class PushHandlerService : GcmServiceBase
	{
		public PushHandlerService() : base(GcmBroadcastReceiver.SENDER_IDS) { }

		const string TAG = "TravelAppeal-GCM";

		public ISharedPreferences prefs;

		protected override void OnRegistered(Context context, string registrationId)
		{

			prefs = MainActivity.Instance.prefs;

			Console.WriteLine("GCM Registered: " + registrationId + "\nKey:" + Android.OS.Build.Serial);

            GlobalStructureInstance.Instance.APNToken = registrationId;

			if(GlobalStructureInstance.Instance.Login)
				SendToken(registrationId,0);
		}

        public void SendToken(string token, int time)
        {
            var client = new RestClient(GlobalStructureInstance.Instance.BaseUrl);

            string cookie = GlobalStructureInstance.Instance.Token;

            if (!string.IsNullOrEmpty(cookie) && !string.IsNullOrWhiteSpace(cookie))
            {
                GlobalStructureInstance.Instance.SendToken = true;

                var request = new RestRequest("push", Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("x-access-token", GlobalStructureInstance.Instance.Token);

                JObject oJsonObject = new JObject();

                /*
                "type": "apn",
                "key": "device-key3",
                "token": "token"
                */

                oJsonObject.Add("type", "android");
                oJsonObject.Add("uuid", token);

                request.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);

                client.ExecuteAsync(request, (sender, e) =>
                {

                    IRestResponse response = sender;

                    MainActivity.Instance.RunOnUiThread(() =>
                    {
                        Console.WriteLine(response.Server + "\nStatusCode:" + response.StatusCode + "\nContent:" + response.Content);
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {

                        }
                        else
                        {

                            if (time <= 5)
                                SendToken(token, time + 1);

                        }
                    });
                });
            }
            else
            {

                if (time <= 5)
                    SendToken(token, time);

            }

        }

		protected override bool OnRecoverableError(Context context, string errorId)
		{

			Console.WriteLine("Recoverable Error: " + errorId);

			return base.OnRecoverableError(context, errorId);
		}

		protected override void OnError(Context context, string errorId)
		{

			Console.WriteLine("GCM Error: " + errorId);
		}
	
		protected override void OnUnRegistered (Context context, string registrationId)
		{
			
			Console.WriteLine( "GCM Unregistered: " + registrationId);

		}

		protected override void OnMessage(Context context, Intent intent)
		{
			Console.WriteLine("GCM Message Received!");

			var msg = new StringBuilder();

			if (intent != null && intent.Extras != null)
			{
				foreach (var key in intent.Extras.KeySet())
					msg.AppendLine(key + "=" + intent.Extras.Get(key).ToString());
			}

			Console.WriteLine("message:\n" + msg);

			string Payload = "";
			bool error = false;
			NotificheObject Not = null;

			try
			{
				
				Payload = intent.Extras.Get("payload").ToString();

				Not = JsonConvert.DeserializeObject<NotificheObject>(Payload);


			}
			catch (Exception e)
			{
				error = true;
				Console.WriteLine("EX:" + e.Message);
			}

			if (!error)
			{

				//Console.WriteLine("\nType" + type + "\nHotel:" + structID + "\nConversation:" + convID);

				bool close2;
				try
				{
					close2 = MainActivity.Instance.IsFinishing;
				}
				catch (Exception e)
				{
					close2 = true;
				}

				if (close2)
				{
					CreateNotification(Not,Payload);
				}
				else
				{
					ManageNotification(Not,Payload);
				}


			}
		}



		public void CreateNotification(NotificheObject Not,string PayloadNot)
		{
			
			var manager =
						(NotificationManager)this.ApplicationContext.GetSystemService(Context.NotificationService);

			Intent intent = null;

			intent = new Intent(this, typeof(SplashPage));

			intent.PutExtra("notification", true);
			intent.PutExtra("NotificationString", PayloadNot);


			string content = Not.Text;
			string title = "";
			if (Not.Type == NotificheType.Fattura)
			{
				title = "Nuova Fattura";
			}

			if (Not.Type == NotificheType.Contratto)
			{
				title = "Scadenza Contratto";
			}

			if (Not.Type == NotificheType.ScadenzaFattura)
			{
				title = "Scadenza Fattura";
			}

			var pendingIntent = PendingIntent.GetActivity(this.ApplicationContext, 0, intent, PendingIntentFlags.UpdateCurrent);

			var builder = new Notification.Builder(this.ApplicationContext)
												.SetDefaults(NotificationDefaults.Sound | NotificationDefaults.Vibrate)
			                              		.SetSmallIcon(Resource.Mipmap.Icon)
												//.SetSound(soundUri)
											 	.SetContentTitle(title)
											  	.SetContentText(content)
										  		//.SetColor(Android.Graphics.Color.ParseColor(ColorUtility.MainColor).ToArgb())
											  	.SetContentIntent(pendingIntent)
											  	.SetAutoCancel(true);

			manager.Notify(1, builder.Build());
		

		}

		public void ManageNotification(NotificheObject Not,string PayloadNot)
		{

			MainActivity.Instance.RunOnUiThread(() =>
			{

				if (MustShowNot())
				{

					var manager =
						(NotificationManager)this.ApplicationContext.GetSystemService(Context.NotificationService);

					Intent intent = null;

					intent = new Intent(this, typeof(NotificationActivity));

					intent.PutExtra("notification", true);
					intent.PutExtra("NotificationString", PayloadNot);

					string content = Not.Text;
					string title = "";
					if (Not.Type == NotificheType.Fattura)
					{
						title = "Nuova Fattura";
					}

					if (Not.Type == NotificheType.Contratto)
					{
						title = "Scadenza Contratto";
					}

					if (Not.Type == NotificheType.ScadenzaFattura)
					{
						title = "Scadenza Fattura";
					}

					var pendingIntent = PendingIntent.GetActivity(this.ApplicationContext, 0, intent, PendingIntentFlags.UpdateCurrent);
                    
					var builder = new Notification.Builder(this.ApplicationContext)
														.SetDefaults(NotificationDefaults.Sound | NotificationDefaults.Vibrate)
														  .SetSmallIcon(Resource.Mipmap.not)
														//.SetSound(soundUri)
														.SetContentTitle(title)
														  .SetContentText(content)
														//.SetColor(Android.Graphics.Color.ParseColor(ColorUtility.MainColor).ToArgb())
														.SetContentIntent(pendingIntent)
														  .SetAutoCancel(true);

					manager.Notify(1, builder.Build());
				

				}
				else
				{
					if (Not.Type == NotificheType.Fattura)
						GlobalStructureInstance.Instance.listDocumenti.Insert(0, Not.Fattura);

					GlobalStructureInstance.Instance.listNotifiche.Insert(0, Not);

					if (GlobalStructureInstance.Instance.isHome)
						HomePage.Instance.DisplayNot(Not);
					if (GlobalStructureInstance.Instance.isDati)
						InfoDataPage.Instance.DisplayNot(Not);
					if (GlobalStructureInstance.Instance.isFattura)
						FatturePage.Instance.DisplayNot(Not);
					if (GlobalStructureInstance.Instance.isContratti)
						ContrattiPage.Instance.DisplayNot(Not);
					if (GlobalStructureInstance.Instance.isNotifiche)
						NotifichePage.Instance.DisplayNot(Not);
				}

			});
		
		}

		public bool MustShowNot()
		{

			List<bool> ListBool = new List<bool>();

			try
			{

				ListBool.Add(HomePage.Instance.isVisible);

			}
			catch (Exception)
			{

				ListBool.Add(false);

			}

			try
			{

				ListBool.Add(ContrattiPage.Instance.isVisible);

			}
			catch (Exception)
			{

				ListBool.Add(false);

			}

			try
			{

				ListBool.Add(FatturePage.Instance.isVisible);

			}
			catch (Exception)
			{

				ListBool.Add(false);

			}

			try
			{

				ListBool.Add(InfoDataPage.Instance.isVisible);


			}
			catch (Exception)
			{

				ListBool.Add(false);

			}

			try
			{
				ListBool.Add(NotifichePage.Instance.isVisible);
			}
			catch (Exception)
			{

				ListBool.Add(false);

			}



			if (!ListBool.Contains(true))
			{
				return true;
			}
			else
			{
				return false;

			}
		}
	}
}

